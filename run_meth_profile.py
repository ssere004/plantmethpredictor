import configs
import meth_profile as mp
import meth_profile_cross_organism as mpc
import meth_profile_cross_context as mpcc
import numpy as np

config_list = [configs.Marchantia]
context_list = ['CG', 'CHG', 'CHH', '']
final_res = mp.experiments(config_list, context_list, dataset_size=50000, window_size=20, coverage_threshold=10)

#res = mpcc.experiments(config_list, [''], dataset_size=55556, window_size=20, coverage_threshold=10)
np.savetxt("meth_profiles_final.csv", final_res, delimiter=", ", fmt ='% s')
#res = mpcc.experiments(config_list, context_list, dataset_size=55556, window_size=20, coverage_threshold=10)

# tr_te_configs = [[configs.Marchantia, configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config],
#                  [configs.Arabidopsis_config, configs.Marchantia],
#                  [configs.Cowpea_config, configs.Marchantia],
#                  [configs.Rice_config, configs.Marchantia],
#                  [configs.Cucumber_config, configs.Marchantia],
#                  [configs.Tomato_config, configs.Marchantia]]
#
# #res = mpcc.cross_cnfg_experiments(tr_te_configs, context_list, dataset_size=55556, window_size=20, coverage_threshold=10)
#
# res = mpcc.cross_context_experiments(config_list, context_list, dataset_size=50000, window_size=20, coverage_threshold=10)


