import numpy as np
import preprocess as preprocess
import profile_generator as pg
import configs as configs
import data_reader
import pandas as pd
import random
import compatibility


config_list = [configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia]
context_list = ['CG', 'CHG', 'CHH']

def get_functional_element_overlap_ratio(config_list, context_list):
    res = []
    for cnfg in config_list:
        for context in context_list:
            organism_name = cnfg['organism_name']
            coverage_threshold = 10
            sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(cnfg, context, coverage_threshold=coverage_threshold)
            if len(context) == 0:
                methylations = preprocess.equal_context_subseter(methylations) ###############################
            methylations_train, methylations_test = preprocess.seperate_methylations(organism_name, methylations, from_file=False)
            methylated_train, unmethylated_train = preprocess.methylations_subseter(methylations_train, 1000)
            ds_size = min(len(methylated_train), len(unmethylated_train))
            step = 500000
            if ds_size * 2 < step: #temporary, does not work for dataset size checking.
                step = (ds_size * 2) - 2
            sample_set = methylated_train[0:int(step/2)]
            profiles, targets = pg.get_profiles(methylations_train, sample_set, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=3200)
            profs = profiles[:, :, 4:]
            sums = np.sum(profs, axis=2)
            sums = np.sum(sums, axis=1)
            np.sum(sums > 0)
            step_res = [organism_name, 'meth', context, float(np.sum(sums > 0))/len(sums)]
            print(step_res)
            res.append(step_res)
            sample_set = unmethylated_train[0:int(step/2)]
            profiles, targets = pg.get_profiles(methylations_train, sample_set, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=3200)
            profs = profiles[:, :, 4:]
            sums = np.sum(profs, axis=2)
            sums = np.sum(sums, axis=1)
            np.sum(sums > 0)
            step_res = [organism_name, 'unmeth', context, float(np.sum(sums > 0))/len(sums)]
            print(step_res)
            res.append(step_res)
    return res

def gene_coverage_percentage(config_list, annotseqs_from_file=True):
    res = []
    for cnfg in config_list:
        organism_name = cnfg['organism_name']
        sequences = data_reader.readfasta(cnfg['seq_address']) #Can get shrinked the size.
        annot_df = data_reader.read_annot(cnfg['annot_address'])
        for at in cnfg['annot_types']:
            annot_subset = preprocess.subset_annot(annot_df, at)
            annot_str = preprocess.make_annotseq_dic(organism_name, at, annot_subset, sequences, from_file=annotseqs_from_file)
            annotated_len = 0
            genome_len = 0
            for key in annot_str.keys():
                df = pd.DataFrame({'template': annot_str[key][:, 0], 'nontemplate': annot_str[key][:, 1]})
                df['annotated'] = df[['template', 'nontemplate']].max(axis=1)
                annotated_len += len(df[df['annotated'] > 0])
                genome_len += len(df)
            step_res = [organism_name, at, float(annotated_len) / genome_len]
            print(step_res)
            res.append(step_res)
    return res

def repeat_coverage_percentage(config_list, annotseqs_from_file=True):
    res = []
    for cnfg in config_list:
        organism_name = cnfg['organism_name']
        sequences = data_reader.readfasta(cnfg['seq_address']) #Can get shrinked the size.
        annot_df = data_reader.read_annot(cnfg['repeat_address'])
        annot_str = preprocess.make_annotseq_dic(organism_name, 'repeat', annot_df, sequences, from_file=annotseqs_from_file)
        annotated_len = 0
        genome_len = 0
        for key in annot_str.keys():
            df = pd.DataFrame({'annotated': annot_str[key][:, 0]})
            annotated_len += len(df[df['annotated'] > 0])
            genome_len += len(df)
        step_res = [organism_name, float(annotated_len) / genome_len]
        print(step_res)
        res.append(step_res)
    return res


def test_meth_enrichment_func(cnfg, annot_df, en_methylations, annot_seqs_onehot):
    annot_df = annot_df.sample(frac=1)
    for i in range(1):
        row = annot_df.iloc[i]
        chr = row['chr']
        start = row['start']
        end = row['end']
        a_type = row['type']
        if a_type not in cnfg['annot_types']:
            continue
        strand = row['strand']
        st_tag = 'temp'
        if strand == '-':
            st_tag = 'nontemp'
        colnm = a_type + '_' + st_tag
        sampled = en_methylations[(en_methylations['chr'] == chr) & (en_methylations['position'] > start) & (en_methylations['position'] < end)]
        for index, row in sampled.iterrows():
            if row[colnm] != 1:
                print('ERRRROOOORRR', row)
    annot_to_num_dic = {}
    for i in range(len(cnfg['annot_types'])):
        annot_to_num_dic[cnfg['annot_types'][i]] = i
    for i in range(1000):
        pos = random.randint(0, len(en_methylations))
        row = en_methylations.iloc[pos]
        p = row['position']
        chr = row['chr']
        ats = cnfg['annot_types']
        for j in range(len(ats)):
            if annot_seqs_onehot[j][chr][p-1, 0] != row[ats[j]+'_temp']:
                print('ERROR', ats[j], 'template', '\n', row)
            if annot_seqs_onehot[j][chr][p-1, 1] != row[ats[j]+'_nontemp']:
                print('ERROR', ats[j], 'template', '\n', row)


#annot_df[(annot_df['chr'] == num_to_chr_dic[5]) & (annot_df['start'] < 6562790) & (annot_df['end'] > 6562790)]

def test_annot_onehot_encode(cnfg, annot_seqs_onehot, annot_df):
    annot_to_num_dic = {}
    for i in range(len(cnfg['annot_types'])):
        annot_to_num_dic[cnfg['annot_types'][i]] = i
    for index, row in annot_df.iterrows():
        start = row['start']
        end = row['end']
        chr = row['chr']
        if row['type'] not in cnfg['annot_types']:
            continue
        if row['strand'] == '+':
            sm = np.sum(annot_seqs_onehot[annot_to_num_dic[row['type']]][chr][start-1:end-1,0])
        else:
            sm = np.sum(annot_seqs_onehot[annot_to_num_dic[row['type']]][chr][start-1:end-1,1])
        if sm != (end-start):
            print('ERROR', row)
            break

def methylation_annotation_enrichment(cnfg, methylations, annot_seqs_onehot, gene_analysis=True):
    #Sort the methylation dataframe, and find the positions where the chromosomes are changed.
    methylations = methylations.sort_values(["chr", "position"], ascending=(True, True))
    chrs_counts = methylations['chr'].value_counts()
    chrs = methylations['chr'].unique()
    chrs = np.sort(chrs)
    first_chr_pos = {chrs[0]: 0}
    chrnums = list(chrs_counts.index)
    sum = 0
    for i in range(len(chrnums)-1):
        if chrs[i] in chrs_counts.keys():
            first_chr_pos[chrs[i+1]] = sum+chrs_counts[chrs[i]]
            sum += chrs_counts[chrs[i]]
    first_chr_pos['l'] = len(methylations)
    chrs= np.concatenate([chrs, ['l']], axis=0)
    # last_chr_pos ==> {0: 5524, 1: 1042784, 2: 1713034, 3: 2550983, 4: 3205486, 5: 4145381, 6: 4153872}
    # methylations.iloc[2550984] => chr 3.0 position    23459763.0
    # methylations.iloc[2550985] => chr 4.0 position    1007
    #The enrich contains the vectors for each type and each strand. These vectors will be added to the dataframe.
    enrich = {}
    at_types = cnfg['annot_types']
    if not gene_analysis:
        at_types = ['repeat']
    for at in at_types:
        enrich[at+'_temp'] = np.zeros(len(methylations))
        enrich[at+'_nontemp'] = np.zeros(len(methylations))
    try:
        for i in range(len(chrs)-1):
            pos = methylations['position'].iloc[first_chr_pos[chrs[i]]: first_chr_pos[chrs[i+1]]] - 1
            chr = methylations.iloc[first_chr_pos[chrs[i]]]['chr']
            at_num = 0
            for at in at_types:
                annot_enr = annot_seqs_onehot[at_num][chr][pos]
                enrich[at + '_temp'][first_chr_pos[chrs[i]]: first_chr_pos[chrs[i+1]]] = annot_enr[:, 0]
                enrich[at + '_nontemp'][first_chr_pos[chrs[i]]: first_chr_pos[chrs[i+1]]] = annot_enr[:, 1]
                at_num += 1
            print(str(i) + 'out of ' + str(len(chrs)) + ' chromosomes are processed')
    except:
        print(i, cnfg['organism_name'], chr, at_num, pos)
    for at in at_types:
        methylations[at + '_nontemp'] = enrich[at+'_nontemp']
        methylations[at + '_temp'] = enrich[at+'_temp']
    return methylations

def make_enrich_meth(cnfg, gene_analysis= True):
    print(cnfg['organism_name'])
    sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(cnfg, '', coverage_threshold=-1)
    methylations = pd.read_table(cnfg['methylation_address'])
    methylations.columns = ['chr', 'position', 'strand', 'meth', 'unmeth', 'context', 'three']
    if cnfg['organism_name'] == configs.Cowpea_config['organism_name']:
        methylations = compatibility.cowpea_methylation_compatibility(methylations)
    if not gene_analysis:
        organism_name = cnfg['organism_name']
        annot_seqs_onehot = []
        annot_df = data_reader.read_annot(cnfg['repeat_address'])
        sequences = data_reader.readfasta(cnfg['seq_address'])
        if organism_name == configs.Cowpea_config['organism_name']:
            sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
            annot_df = compatibility.cowpea_annotation_compatibility(annot_df)
        annot_str = preprocess.make_annotseq_dic(organism_name, 'repeat', annot_df, sequences, from_file=True, strand_spec=False)
        annot_res = {}
        for chr_ in annot_str.keys():
            tmp = np.zeros((annot_str[chr_].shape[0], 2))
            tmp[:, 0] = annot_str[chr_][:, 0]
            annot_res[chr_] = tmp
        annot_seqs_onehot.append(annot_res)
    en_methylations = methylation_annotation_enrichment(cnfg, methylations, annot_seqs_onehot, gene_analysis=gene_analysis)
    print('saving started for ' + cnfg['organism_name'])
    #if gene_analysis:
    #   en_methylations.to_csv('./dump_files/'+cnfg['organism_name'] +'_en_methylations.txt', index=None, header=None, sep='\t')
    return en_methylations

# res = []
# context_list = ['CG', 'CHG', 'CHH']
# for cnfg in config_list:
#     organism_name = cnfg['organism_name']
#     en_methylations = pd.read_table('./dump_files/'+organism_name+'_en_methylations.txt', header=None)
#     columns = ['chr', 'position', 'strand', 'meth', 'unmeth', 'context', 'three']
#     for at in cnfg['annot_types']:
#         columns.append(at + '_nontemp')
#         columns.append(at + '_temp')
#     en_methylations.columns = columns
#     annot_df = data_reader.read_annot(cnfg['annot_address'])
#     for at in cnfg['annot_types']:
#         annot_su = annot_df[annot_df['type'] == at]
#         sum = 0
#         for index, row in annot_su.iterrows():
#             sum += row['end']-row['start']
#         print(organism_name, at +' coverage percentage '  , float(sum)/cnfg['genome_size'])
#         res.append([organism_name, at, float(sum)/cnfg['genome_size']])
#     for at in cnfg['annot_types']:
#         for context in context_list:
#             total = en_methylations[en_methylations['context'] == context]
#             frac = total[(total[at+'_temp'] == 1)|(total[at+'_nontemp'] == 1)]
#             print(organism_name + ' '+at+' '+context+' ' + str(float(len(frac)/ len(total))))
#             res.append([organism_name, at, context, str(float(len(frac)/len(total)))])
#     np.savetxt("count_annot.csv", res, delimiter=", ", fmt ='% s')


def find_coverage_percentage(sequences, annot_df):
    sum = 0
    for chr in sequences.keys():
        sum+=len(sequences[chr])
    seq_annot = np.zeros(sum)
    chrs = list(sequences.keys())
    chrs.sort()
    keep = 0
    start_chrs = {}
    for i in range(len(chrs)):
        start_chrs[chrs[i]] = keep
        keep+=len(sequences[chrs[i]])
    for index, row in annot_df.iterrows():
        start = start_chrs[row['chr']] + row['start']
        end = start_chrs[row['chr']] + row['end']
        seq_annot[start:end] = 1
    return np.sum(seq_annot) / sum

# res = []
# for cnfg in config_list:
#     sequences = data_reader.readfasta(cnfg['seq_address'])
#     organism_name = cnfg['organism_name']
#     annot_df = data_reader.read_annot(cnfg['annot_address'])
#     if organism_name == configs.Cowpea_config['organism_name']:
#         sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
#         annot_df = compatibility.cowpea_annotation_compatibility(annot_df)
#     for at in cnfg['annot_types']:
#         annot_su = annot_df[annot_df['type'] == at]
#         step_res = [cnfg['organism_name'], at, find_coverage_percentage(sequences, annot_su)]
#         print(step_res)
#         res.append(step_res)



# res = gene_coverage_percentage(config_list, annotseqs_from_file=True)
# np.savetxt("GFG_coverage_count.csv", res, delimiter=", ", fmt='% s')
#
#
# pos =methylations['position'].iloc[first_chr_pos[i]: first_chr_pos[i+1]]
# chr = num_to_chr_dic[methylations.iloc[first_chr_pos[i]]['chr']]
# at_num = 0
# annot_enr = annot_seqs_onehot[at_num][chr][pos]
# enrich[at+'_temp'] = np.concatenate([enrich[at+'_temp'], annot_enr[:, 0]], axis=0)
# len(enrich[at+'_temp'])
#
# def reverse_num_to_chr(num_to_chr_dic):
#     rev = {}
#     for key in num_to_chr_dic.keys():
#         rev[num_to_chr_dic[key]] = key
#     return rev
#
#
# en_methylations[(en_methylations['chr'] == rev['NC_003074.8']) & (en_methylations['position'] > 22481879) & (en_methylations['position'] < 22484250)]

res = []
for cnfg in config_list:
    en_methylations = make_enrich_meth(cnfg, gene_analysis=False)
    for context in context_list:
        for fe in ['repeat']:
            sub_meth = en_methylations[en_methylations['context'] == context]
            cntx_size = len(sub_meth)
            frac_size = len(sub_meth[sub_meth[fe+'_nontemp']+sub_meth[fe+'_temp']>0])
            res.append([cnfg['organism_name'], context, str(frac_size/cntx_size)])
np.savetxt("temp_.csv", res, delimiter=", ", fmt='% s')


################################################################################################
# res = []
# sub_meth_me = en_methylations[en_methylations['meth']/(en_methylations['meth'] + en_methylations['unmeth']) > 0.5]
# sub_meth_ume = en_methylations[en_methylations['meth']/(en_methylations['meth'] + en_methylations['unmeth']) <= 0.5]
#
# res.append([
#     len(sub_meth_me[sub_meth_me['context'] == 'CG'])/ len(sub_meth_me),
#     len(sub_meth_me[sub_meth_me['context'] == 'CHG'])/ len(sub_meth_me),
#     len(sub_meth_me[sub_meth_me['context'] == 'CHH'])/ len(sub_meth_me),
#             ])
#
# res.append([
#     len(sub_meth_ume[sub_meth_ume['context'] == 'CG'])/ len(sub_meth_ume),
#     len(sub_meth_ume[sub_meth_ume['context'] == 'CHG'])/ len(sub_meth_ume),
#     len(sub_meth_ume[sub_meth_ume['context'] == 'CHH'])/ len(sub_meth_ume),
#             ])
# ####################################################################################################
# sub_meth = en_methylations[en_methylations.meth + en_methylations.unmeth > 10]
# res = []
# for context in context_list:
#     sub_con = sub_meth[sub_meth.context == context]
#     res.append(len(sub_con[sub_con.meth/(sub_con.meth+sub_con.unmeth) > 0.5])/ len(sub_con))
#
# res.append(len(sub_meth[sub_meth.meth/(sub_meth.meth+sub_meth.unmeth) > 0.5])/ len(sub_meth))
