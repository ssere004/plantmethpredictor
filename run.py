import configs as configs
import profile_generator as pg
#import preprocess as preprocess
#import motif_finding as mf
import meth_seq_comb2 as msc

chr = 'NC_003075.7'

import numpy as np
config_list = [configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia]
config_list = [configs.Marchantia]
context_list = ['CG', 'CHG', 'CHH', '']
window_size = 3200
window_sizes = [100, 200, 400, 800, 1600, 3200, 6400]
block_sizes = [(10, 10), (20, 10), (20, 20), (40, 20), (40, 40), (80, 40), (80, 80)]
final_res = []

steps = [0, 40000, 80000, 120000, 200000, 400000, 600000, 800000, 1000000]

# config_list = [configs.Arabidopsis_config, configs.Marchantia]
#
# res = pg.run_experiments(config_list, context_list, [3200], [(80, 40)], [0, 500000], coverage_threshold=10, include_annot=False, include_repeat=False)
# np.savetxt("res_gpu0.csv", res, delimiter=", ", fmt='% s')


#For motif finding
# rc = configs.run_config
# config_list = [configs.Cowpea_config]
# for cnfg in config_list:
#     for context in ['CHH', '']:
#         rc['context'] = context
#         mf.find_motifs(cnfg, rc, use_trained_models=True)

#for at in configs.Arabidopsis_config['annot_types']:

res = msc.experiments(config_list, context_list, dataset_size=50000, meth_window_size=20, seq_window_size=3200, coverage_threshold=10)
np.savetxt("GFG_meth_seq_comb.csv", res, delimiter=", ", fmt='% s')

# res6 = []
# for i in range(6):
#    res = pg.run_experiments([configs.Arabidopsis_config], ['CG', 'CHG', 'CHH', ''], [3200], [(80, 40)], [0, 500000], coverage_threshold=10, include_annot=True, include_repeat=True)
#    for rr in res:
#        res6.append(rr)
#    np.savetxt("GFG_annot_window_arabidopsis0.csv", res6, delimiter=", ", fmt='% s')
#
#
# res6 = []
# for i in range(2):
#    res = pg.run_experiments([configs.Arabidopsis_config], ['CG', 'CHG', 'CHH', ''], [3200], [(80, 40)], steps, coverage_threshold=10, include_annot=True)
#    for rr in res:
#        res6.append(rr)
#    np.savetxt("GFG_annot_datasize_arabidopsis6.csv", res6, delimiter=", ", fmt='% s')




