import profile_generator as pg

import preprocess as preprocess
import random
from datetime import datetime
import tensorflow as tf
import numpy as np
import pandas as pd
from tensorflow import keras
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score, precision_score, recall_score
import configs as configs
import data_reader
import compatibility
import os


model_names = [
    "TomatoCHHannot4500003200.mdl",
    "TomatoCHGannot4500003200.mdl",
    "TomatoCGannot4114293200.mdl",
    'Tomatoannot4500003200.mdl',
    "CucumberCHHannot4500003200.mdl",
    "CucumberCHGannot4500003200.mdl",
    "CucumberCGannot4500003200.mdl",
    'Cucumberannot4500003200.mdl',
    "RiceCHHannot4500003200.mdl",
    "RiceCHGannot4500003200.mdl",
    "RiceCGannot4500003200.mdl",
    'Riceannot4500003200.mdl',
    "CowpeaCHHannot4500003200.mdl",
    "CowpeaCHGannot4500003200.mdl",
    "CowpeaCGannot4500003200.mdl",
    'Cowpeaannot4500003200.mdl',
    "ArabidopsisCHHannot1726883200.mdl",
    "ArabidopsisCHGannot4500003200.mdl",
    "ArabidopsisCGannot4500003200.mdl",
    'Arabidopsisannot4500003200.mdl'
]

model_names = ['c24CGannot4500003200.mdl',
'c24CGannot4500003200.mdl',
'c24CHGannot2062673200.mdl',
'c24CHHannot1982263200.mdl',
'c24combinedannot4500003200.mdl',
'col0CGannot4500003200.mdl',
'col0CHGannot3485573200.mdl',
'col0CHHannot3943263200.mdl',
'col0combinedannot4500003200.mdl']


model_names = [
    'ArabidopsisCG4999983200.mdl',
    'ArabidopsisCHG4999983200.mdl',
    'ArabidopsisCHH1727353200.mdl',
    'Arabidopsiscombinedannot:False-repeat:False450000_3200.mdl',
    'CowpeaCG4999983200.mdl',
    'CowpeaCHG4999983200.mdl',
    'CowpeaCHH4999983200.mdl',
    'Cowpeacombined4050003200.mdl',
    'CucumberCG4999983200.mdl',
    'CucumberCHG4999983200.mdl',
    'CucumberCHH4999983200.mdl',
    'Cucumbercombined4050003200.mdl',
    'MarchantiaCGannot:False-repeat:False450000_3200.mdl',
    'MarchantiaCHGannot:False-repeat:False450000_3200.mdl',
    'MarchantiaCHH703353200.mdl',
    'Marchantiacombined2103463200.mdl',
    'RiceCG4999983200.mdl',
    'RiceCHG4999983200.mdl',
    'RiceCHH4999983200.mdl',
    'Ricecombined4050003200.mdl',
    'TomatoCG499983200.mdl',
    'TomatoCHG4999983200.mdl',
    'TomatoCHH4999983200.mdl',
    'Tomatocombined4050003200.mdl'
]

model_names = os.listdir('./models/gr_v1/')

context_list = ['CG', 'CHG', 'CHH', '']
cnfgs = [configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia]
include_annot = True
include_repeat = True
window_size = 3200


def train(cnfg, context, methylations_train, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, include_annot=True, memory_chunk_size=10000):
    organism_name = cnfg['organism_name']
    window_sizes = [3200]
    block_sizes = [(80, 40)]
    w = 0
    steps = [0, 450000]
    s = 0
    if not include_annot:
        del annot_seqs_onehot
        annot_seqs_onehot = []
    PROFILE_ROWS = window_sizes[w]
    PROFILE_COLS = 4 + 2*len(annot_seqs_onehot)
    model = Sequential()
    model.add(Conv2D(16, kernel_size=(1, PROFILE_COLS), activation='relu', input_shape=(PROFILE_ROWS, PROFILE_COLS, 1)))
    model.add(Reshape((block_sizes[w][0], block_sizes[w][1], 16), input_shape=(PROFILE_ROWS, 1, 16)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    print('model processed')
    print(datetime.now())
    opt = tf.keras.optimizers.SGD(lr=0.01)
    model.compile(loss=keras.losses.binary_crossentropy, optimizer=opt, metrics=['accuracy'])
    methylated_train, unmethylated_train = preprocess.methylations_subseter(methylations_train, window_sizes[w])
    ds_size = min(len(methylated_train), len(unmethylated_train))
    x_train_sz = 0
    step = steps[s+1] - steps[s]
    print('##################################', step)
    print('#################################', ds_size)
    if ds_size * 2 < steps[s+1]: #temporary, does not work for dataset size checking.
        step = (ds_size * 2) - 2
    slice = int(steps[s]/2)
    for chunk in range(slice, slice+int(step/2), memory_chunk_size):
        if chunk+memory_chunk_size > slice+int(step/2):
            sample_set = methylated_train[chunk:slice+int(step/2)]+unmethylated_train[chunk:slice+int(step/2)]
        else:
            sample_set = methylated_train[chunk:chunk+memory_chunk_size]+unmethylated_train[chunk:chunk+memory_chunk_size]
        random.shuffle(sample_set)
        profiles, targets = pg.get_profiles(methylations_train, sample_set, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=window_sizes[w])
        X, Y = pg.data_preprocess(profiles, targets, include_annot=include_annot)
        x_train, x_val, y_train, y_val = pg.split_data(X, Y, pcnt=0.1)
        x_train_sz += len(x_train)
        with tf.device('/device:GPU:0'):
            print('model fitting started for ' + organism_name + ' ' + context)
            print(datetime.now())
            model.fit(x_train, y_train, batch_size=32, epochs=45, verbose=0, validation_data=(x_val, y_val))
            print('model fitting ended for ' + str(len(x_train)) + ' data')
            print(datetime.now())
            del x_train, y_train
    ia_tag = ''
    if include_annot:
        ia_tag = 'annot'
    if len(context) != 0:
        model_tag = str(organism_name) + '_' + str(context) + '_' + ia_tag + '_' + str(x_train_sz) + '_' + str(window_sizes[w]) + '.mdl'
    else:
        model_tag = str(organism_name) + 'combined' + ia_tag + str(x_train_sz) + str(window_sizes[w]) + '.mdl'
    model.save('./models/' + model_tag)
    return './models/' + model_tag


trained_models = {}
cnfgs = [configs.Arabidopsis_config, configs.Arabidopsis_c24_config]
for cnfg in cnfgs:
    organism_name = cnfg['organism_name']
    for context in context_list:
        for model in model_names:
            if context == '':
                context = 'combined'
            if organism_name in model and context in model:
                trained_models[organism_name + context] = keras.models.load_model('./models/gr_v1/' + model)



res = []

tr_te_configs = [[configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia],
                 [configs.Cowpea_config, configs.Arabidopsis_config,  configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia],
                 [configs.Rice_config, configs.Arabidopsis_config, configs.Cowpea_config,  configs.Cucumber_config, configs.Tomato_config, configs.Marchantia],
                 [configs.Cucumber_config, configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config,  configs.Tomato_config, configs.Marchantia],
                 [configs.Tomato_config, configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config,  configs.Marchantia],
                 [configs.Marchantia, configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config]]


tr_te_configs = [[configs.Arabidopsis_config, configs.Arabidopsis_c24_config],
                [configs.Arabidopsis_c24_config, configs.Arabidopsis_config]]

for cnfgss in tr_te_configs:
    tr_config = cnfgss[0]
    tr_organism_name = tr_config['organism_name']
    for context in context_list:
        if context != '':
            model = trained_models[tr_organism_name+context]
        else:
            model = trained_models[tr_organism_name+'combined']
        for te_config in cnfgss[1:]:
            te_organism_name = te_config['organism_name']
            if te_organism_name == tr_organism_name:
                continue
            sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(te_config, context, coverage_threshold=10)
            if len(context) == 0:
                methylations = preprocess.equal_context_meth_subseter(methylations)
            if include_repeat:
                annot_df = data_reader.read_annot(te_config['repeat_address'])
                sequences = data_reader.readfasta(te_config['seq_address'])
                if te_organism_name == configs.Cowpea_config['organism_name']:
                    sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
                    annot_df = compatibility.cowpea_annotation_compatibility(annot_df)
                annot_str = preprocess.make_annotseq_dic(te_organism_name, 'repeat', annot_df, sequences, from_file=True, strand_spec=False)
                annot_seqs_onehot.append(annot_str)
            x_test, y_test = pg.test_sampler(methylations, sequences_onehot, annot_seqs_onehot, window_size, num_to_chr_dic, include_annot=include_annot)
            tag = 'seq-only'
            if include_annot:
                tag = 'seq-annot'
            y_pred = model.predict(x_test)
            del sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic
            step_res = [tr_organism_name, te_organism_name, context, tag, 3200, 500000, len(x_test), accuracy_score(y_test, y_pred.round()),
                                f1_score(y_test, y_pred.round()), precision_score(y_test, y_pred.round()), recall_score(y_test, y_pred.round())]
            res.append(step_res)
            np.savetxt("GFG_cross_accession.csv", res, delimiter=", ", fmt ='% s')



