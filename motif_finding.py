import numpy as np
import tensorflow as tf
import pandas as pd
from sklearn.cluster import KMeans
import profile_generator as pg
import preprocess
import random as random
from tensorflow import keras
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape
import matplotlib.pyplot as plt
import sys


model_names = [
    'ArabidopsisCG4999983200.mdl',
    'ArabidopsisCHG4999983200.mdl',
    'ArabidopsisCHH1727353200.mdl',
    'Arabidopsiscombinedannot:False-repeat:False450000_3200.mdl',
    'CowpeaCG4999983200.mdl',
    'CowpeaCHG4999983200.mdl',
    'CowpeaCHH4999983200.mdl',
    'Cowpeacombined4050003200.mdl',
    'CucumberCG4999983200.mdl',
    'CucumberCHG4999983200.mdl',
    'CucumberCHH4999983200.mdl',
    'Cucumbercombined4050003200.mdl',
    'MarchantiaCGannot:False-repeat:False450000_3200.mdl',
    'MarchantiaCHGannot:False-repeat:False450000_3200.mdl',
    'MarchantiaCHH703353200.mdl',
    'Marchantiacombined2103463200.mdl',
    'RiceCG4999983200.mdl',
    'RiceCHG4999983200.mdl',
    'RiceCHH4999983200.mdl',
    'Ricecombined4050003200.mdl',
    'TomatoCG499983200.mdl',
    'TomatoCHG4999983200.mdl',
    'TomatoCHH4999983200.mdl',
    'Tomatocombined4050003200.mdl'
]



def find_motifs(cnfg, rc, window_size=3200, block_size=(80, 40), data_size=500000, use_trained_models=False):
    organism_name = cnfg['organism_name']
    sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(cnfg, rc['context'], coverage_threshold=rc['coverage_threshold'])
    if len(rc['context']) == 0:
        methylations = preprocess.equal_context_meth_subseter(methylations)
        rc['context'] = 'combined'
    print('ORGANISM NAME', organism_name, 'Mehtylation size', len(methylations))
    methylations_train, methylations_test = preprocess.seperate_methylations(organism_name, methylations, from_file=False)
    if use_trained_models:
        model = get_model(cnfg, rc['context'])
        if model == None:
            raise ValueError('Could not find model for organism_name ' + cnfg['organism_name'] +' and context ' + rc['context'])
    else:
        PROFILE_ROWS = window_size
        annot_seqs_onehot = []
        PROFILE_COLS = 4
        with tf.device('/device:GPU:0'):
            model = Sequential()
            model.add(Conv2D(16, kernel_size=(1, PROFILE_COLS), activation='relu', input_shape=(PROFILE_ROWS, PROFILE_COLS, 1)))
            model.add(Reshape((block_size[0], block_size[1], 16), input_shape=(PROFILE_ROWS, 1, 16)))
            model.add(Flatten())
            model.add(Dense(128, activation='relu'))
            model.add(Dropout(0.5))
            model.add(Dense(1, activation='sigmoid'))
            print('model processed')
            opt = tf.keras.optimizers.SGD(lr=0.01)
            model.compile(loss=keras.losses.binary_crossentropy, optimizer=opt, metrics=['accuracy'])
        methylated_train, unmethylated_train = preprocess.methylations_subseter(methylations_train, window_size)
        ds_size = int(min(len(methylated_train), len(unmethylated_train), data_size/2)*2)
        sample_set = methylated_train[0:int(ds_size/2)]+unmethylated_train[0:int(ds_size/2)]
        random.shuffle(sample_set)
        profiles, targets = pg.get_profiles(methylations_train, sample_set, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=window_size)
        X, Y = pg.data_preprocess(profiles, targets, include_annot=False)
        x_train, x_val, y_train, y_val = pg.split_data(X, Y, pcnt=0.1)
        x_train_sz = len(x_train)
        print('model fitting ended for ' + str(x_train_sz) + ' data')
        chunk_num = 10
        chunk_size_tr = int(len(x_train)/chunk_num)
        chunk_size_val = int(len(x_val)/chunk_num)
        for i in range(chunk_num):
            with tf.device('/device:GPU:0'):
                model.fit(x_train[i*chunk_size_tr:(i+1)*chunk_size_tr], y_train[i*chunk_size_tr:(i+1)*chunk_size_tr], batch_size=32, epochs=45, verbose=0, validation_data=(x_val[i*chunk_size_val:(i+1)*chunk_size_val], y_val[i*chunk_size_val:(i+1)*chunk_size_val]))
        del x_train, y_train
    x_test, y_test = pg.test_sampler(methylations_test, sequences_onehot, annot_seqs_onehot, window_size, num_to_chr_dic, include_annot=False, test_sample_size=50000)
    y_pred = model.predict(x_test)
    if len(y_test.shape) > 1:
        y_test = y_test[:, 0]
    if len(y_pred.shape) > 1:
        y_pred = y_pred[:, 0]
    tp = x_test[(y_pred.round() == y_test) & (y_test > 0), :, :]
    tn = x_test[(y_pred.round() == y_test) & (y_test == 0), :, :]
    tp = tp[:min(10000, len(tp)), :, :]
    tn = tn[:min(10000, len(tn)), :, :]
    # save_motif_heatmap(model, tp, organism_name[:2]+'_'+rc['context'] + '_methylated.csv')
    # save_motif_heatmap(model, tn, organism_name[:2]+'_'+rc['context'] + '_unmethylated.csv')
    for motif_size in [10, 30, 50]:
        file_name = './motifs/' + organism_name+'_' + rc['context'] + '_' + 'motif_size:' + str(motif_size)+'_methylated.fa'
        save_motif_fasta_files(model, tp, motif_size, file_name)
        file_name = './motifs/' + organism_name+'_' + rc['context'] + '_' + 'motif_size:' + str(motif_size)+'_unmethylated.fa'
        save_motif_fasta_files(model, tn, motif_size, file_name)
        #save_motif_heatmap(model, tn, file_name)
        print(motif_size, 'done')

def get_model(cnfg, context):
    organism_name = cnfg['organism_name']
    for model in model_names:
        if context == '':
            context = 'combined'
        if organism_name in model and context in model:
            return keras.models.load_model('./models/' + model)
    return None

def seq_only_motif_finding(model, X, motif_size, return_type='motif'):
    last_conv_layer_name = list(filter(lambda x: isinstance(x, keras.layers.Conv2D), model.layers))[-1].name
    last_conv_layer = model.get_layer(last_conv_layer_name)
    last_conv_layer_model = tf.keras.Model(model.inputs, last_conv_layer.output)
    classifier_input = tf.keras.Input(shape=last_conv_layer.output.shape[1:])
    layers = list(filter(lambda x: not isinstance(x, keras.layers.Conv2D), model.layers))
    layer_names = [l.name for l in layers]
    x = classifier_input
    for layer_name in layer_names:
        x = model.get_layer(layer_name)(x)
    classifier_model = tf.keras.Model(classifier_input, x)
    with tf.GradientTape() as tape:
        inputs = X[np.newaxis, ...]
        last_conv_layer_output = last_conv_layer_model(inputs)
        tape.watch(last_conv_layer_output)
        preds = classifier_model(last_conv_layer_output)
        top_pred_index = tf.argmax(preds[0])
        top_class_channel = preds[:, top_pred_index]
    grads = tape.gradient(top_class_channel, last_conv_layer_output)
    pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))
    last_conv_layer_output = last_conv_layer_output.numpy()[0]
    pooled_grads = pooled_grads.numpy()
    for i in range(pooled_grads.shape[-1]):
        last_conv_layer_output[:, :, i] *= pooled_grads[i]
    gradcam = np.mean(last_conv_layer_output, axis=-1)
    if return_type == 'motif':
        rwa = pd.Series(gradcam[:, 0]).rolling(motif_size).mean()
        return X[rwa.idxmax() - motif_size + 1: rwa.idxmax()+1]
    elif return_type == 'cam-vec':
        return gradcam
    else:
        return None

def save_motif_heatmap(model, X, file_name):
    res = np.zeros((len(X), X.shape[1]))
    for i in range(len(X)):
        res[i] = seq_only_motif_finding(model, X[i], -1, return_type='cam-vec')[:, 0]
    np.savetxt('./heatmap/'+file_name, res, delimiter=',')

def save_motif_fasta_files(model, X, motif_size, file_name):
    res = np.zeros((len(X), motif_size, X.shape[2]))
    for i in range(len(X)):
        res[i] = seq_only_motif_finding(model, X[i], motif_size)[:, :, 0]
    f = open(file_name, "w")
    try:
        for i in range(len(res)):
            seq = convert_onehot_to_seq(res[i])
            if len(seq) == int(motif_size):
                f.write('>'+str(i)+'\n')
                f.write(seq)
                f.write("\n")
    except:
        print('Something went wrong', seq, i)
    finally:
        f.close()


def convert_onehot_to_seq(arr):
    try:
        tmp = np.where(arr)[1]
        tmp = tmp.astype(str)
        tmp = np.char.replace(tmp, '0', 'A')
        tmp = np.char.replace(tmp, '1', 'C')
        tmp = np.char.replace(tmp, '2', 'G')
        tmp = np.char.replace(tmp, '3', 'T')
        return ''.join(tmp)
    except:
        return ''


def min_max_scaling(df):
    df_norm = df.copy()
    for column in df_norm.columns:
        df_norm[column] = (df_norm[column] - df.min().min()) / (df.max().max() - df.min().min())
    return df_norm

def plot_heatmap(fn):
    png_fn = fn[:-3]+'_'
    df = pd.read_csv(fn, header=None)
    pre_filter = len(df)
    df = df[(df == 0).astype(int).sum(axis=1) != df.shape[1]]
    print(str(pre_filter - len(df)) +' Rows Filtered because all of there were zero 0')
    df[df < -0.0005] = -0.0005
    df[df > 0.0005] = 0.0005
    plt.legend('', frameon=False)
    fig1, ax1 = plt.subplots()
    ax1.imshow(df, cmap="RdYlBu")
    fig1.savefig(png_fn + '1.png', dpi=300)
    df = min_max_scaling(df)
    plt.close()
    plt.cla()
    plt.clf()
    fig2, ax2 = plt.subplots()
    ax2.imshow(df, cmap ="RdYlBu")
    fig2.savefig(png_fn + '2.png', dpi=300)
    plt.close()
    plt.cla()
    plt.clf()
    df[df < 0.49] = 0
    df[df > 0.51] = 1
    fig3, ax3 = plt.subplots()
    ax3.imshow(df, cmap ="RdYlBu")
    fig3.savefig(png_fn + '2.png', dpi=300)
    plt.close()
    plt.cla()
    plt.clf()
    sys.setrecursionlimit(100000)
    # g = sb.clustermap(df)
    # g.savefig(png_fn + '3.png')
    # plt.close()
    # plt.cla()
    # plt.clf()

def read_tsv(fn):
    tsv = pd.read_csv(fn, sep='\t')
    return tsv[~tsv.Target_ID.isna()]

def find_common_in_all(tsvs):
    if len(tsvs) == 0:
        raise ValueError('list of tsvs is empty')
    res = set(list(tsvs[0]['Target_ID']))
    for tsv in tsvs[1:]:
        res = res & set(list(tsv['Target_ID']))
    return res

def find_common_in_meth_unmeth(tsvs_meth, tsvs_unmeth, organism_names):
    if len(tsvs_meth) != len(tsvs_unmeth) or len(tsvs_meth) != len(organism_names):
        raise ValueError("tsv files and organism_names are not the same length")
    res = {}
    for i in range(len(tsvs_meth)):
        res[organism_names[i]] = set(list(tsvs_meth[i]['Target_ID'])) & set(list(tsvs_unmeth[i]['Target_ID']))
    return res

def get_file_name(organism_name, context, is_meth):
    if is_meth:
        return 'm_' + organism_name[:2] + '_' + context+'_50.tsv'
    else:
        return 'um_' + organism_name[:2] + '_' + context+'_50.tsv'


# res_dic = {}
#
# organism_names = ['Arabidopsis', 'Cowpea', 'Rice', 'Cucumber', 'Tomato', 'Marchantia']
# contexts = ['CG', 'CHG', 'CHH']
#
# for organism in organism_names:
#     tsvs = []
#     for context in contexts:
#         tsvs.append(read_tsv(get_file_name(organism, context, True)))
#         tsvs.append(read_tsv(get_file_name(organism, context, False)))
#     res = find_common_in_all(tsvs)
#     res_dic[organism] = res
#     tsvs = []
#     for context in contexts:
#         tsvs.append(read_tsv(get_file_name(organism, context, True)))
#     res = find_common_in_all(tsvs)
#     res_dic['m_'+organism] = res
#     tsvs = []
#     for context in contexts:
#         tsvs.append(read_tsv(get_file_name(organism, context, False)))
#     res = find_common_in_all(tsvs)
#     res_dic['um_'+organism] = res
#
# tsvs = []
# for organism in organism_names:
#     for context in contexts:
#         #tsvs.append(read_tsv(get_file_name(organism, context, True)))
#         tsvs.append(read_tsv(get_file_name(organism, context, False)))
#
#
#
# res = find_common_in_all(tsvs)
# len(res)
#
#
# for organism in organism_names:
#     print(len(res_dic[organism]))
#     print(len(res_dic['m_' + organism]))
#     print(len(res_dic['um_' + organism]))
