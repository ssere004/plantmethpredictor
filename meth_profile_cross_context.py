import random
import numpy as np
from tensorflow import keras
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dropout, Flatten, Reshape
from tensorflow.keras.optimizers import SGD
from sklearn.metrics import accuracy_score
import profile_generator as pg
import tensorflow as tf
import configs as configs
from os.path import exists
import preprocess
import pandas as pd
#import test_cross_context as tcc

#Making two dataFrames 1)Each context, methylation status has size/6 entries in the dataframe 2)remaining
def meth_dfs_construction(methylations, size):
    meth_subs_dic = cntx_spc_sub_methylations(methylations)
    eq_dfs = []
    res_dfs = []
    for cntx in meth_subs_dic.keys():
        sampled_df, residue_df = sample_n_subtract(meth_subs_dic[cntx], int(size/6))
        eq_dfs.append(sampled_df)
        res_dfs.append(residue_df)
    return pd.concat(eq_dfs).sample(frac=1), pd.concat(res_dfs).sample(frac=1)

def sample_n_subtract(methylations, num):
    methylations = methylations.sample(frac=1).reset_index(drop=True)
    return methylations.iloc[:num], methylations.iloc[num:]

def cntx_spc_sub_methylations(methylations, threshold=0.5):
    cg_meth = methylations[(methylations['context'] == 'CG') & (methylations['mlevel'] >= threshold)]
    cg_unmeth = methylations[(methylations['context'] == 'CG') & (methylations['mlevel'] < threshold)]
    chg_meth = methylations[(methylations['context'] == 'CHG') & (methylations['mlevel'] >= threshold)]
    chg_unmeth = methylations[(methylations['context'] == 'CHG') & (methylations['mlevel'] < threshold)]
    chh_meth = methylations[(methylations['context'] == 'CHH') & (methylations['mlevel'] >= threshold)]
    chh_unmeth = methylations[(methylations['context'] == 'CHH') & (methylations['mlevel'] < threshold)]
    return {'cg_meth': cg_meth, 'cg_unmeth': cg_unmeth,
            'chg_meth': chg_meth, 'chg_unmeth': chg_unmeth,
            'chh_meth': chh_meth, 'chh_unmeth': chh_unmeth}

def check_indexes(methylations, sample_methylations):
    for i in range(1000):
        p = random.randint(0, len(sample_methylations))
        row1 = sample_methylations.iloc[p]
        idx = row1['idx']
        row2 = methylations[methylations['idx'] == idx]
        if len(row2) > 1:
            print(row2)
            return False
        row2 = row2.iloc[0]
        for c in methylations.columns:
            if row1[c] != row2[c]:
                print('ERROR', row2, row1)
                return False
    return True

def check_mlevels(methylations, mlevels):
    for i in range(1000):
        p = random.randint(0, len(methylations))
        row = methylations.iloc[p]
        if row['mlevel'] != mlevels[row['idx']]:
            print('ERRROOORR', row)

def input_maker(methylations, sample_methylations, datasize, window_size, organism_name, from_file, half_w, threshold=0.5):
    mlevels = methylations['mlevel']
    mlevels = np.asarray(mlevels)
    chrs_counts = methylations['chr'].value_counts()
    last_chr_pos = {}
    chrnums = list(chrs_counts.index)
    sum = 0
    for i in range(len(chrnums)):
        if i in chrs_counts.keys():
            last_chr_pos[i] = sum+chrs_counts[i]-1
            sum += chrs_counts[i]
    # last_chr_pos ==> {0: 5524, 1: 1042784, 2: 1713034, 3: 2550983, 4: 3205486, 5: 4145381, 6: 4153872}
    # methylations.iloc[2550983] => chr 3.0 position    23459763.0
    # methylations.iloc[2550984] => chr 4.0 position    1007
    sub_methylations_me = sample_methylations[sample_methylations['mlevel'] >= threshold]
    sub_methylations_ume = sample_methylations[sample_methylations['mlevel'] < threshold]
    idxs_me = sub_methylations_me['idx']
    idxs_ume = sub_methylations_ume['idx']
    #if not check_indexes(methylations, sub_methylations_me) or not check_indexes(methylations, sub_methylations_ume):
    #    exit()
    X = np.zeros((datasize, window_size))
    Y = np.zeros(datasize)
    if from_file and exists('./temporary_files/' +organism_name+'_meth_avlbls.npy'):
        avlbls_me = np.load('./temporary_files/' +organism_name+'_meth_avlbls_me.npy')
        avlbls_ume = np.load('./temporary_files/' +organism_name+'_meth_avlbls_ume.npy')
    else:
        avlbls_me = np.asarray(idxs_me)
        avlbls_ume = np.asarray(idxs_ume)
        for lcp in list(last_chr_pos.values()):
            if lcp > 0 and lcp < len(mlevels) - window_size:
                avlbls_me = np.setdiff1d(avlbls_me, range(lcp-half_w, lcp+half_w))
                avlbls_ume = np.setdiff1d(avlbls_ume, range(lcp-half_w, lcp+half_w))
        np.save('./temporary_files/' +organism_name+'_meth_avlbls_me.npy', avlbls_me)
        np.save('./temporary_files/' +organism_name+'_meth_avlbls_ume.npy', avlbls_ume)
    #check_availables
    smple_me = random.sample(list(avlbls_me), int(datasize/2))
    smple_ume = random.sample(list(avlbls_ume), int(datasize/2))
    smple = np.concatenate([np.asarray(smple_me), np.asarray(smple_ume)])
    random.shuffle(smple)
    count_errored = 0
    #check_mlevels(methylations, mlevels)
    print('border conditions: ', np.count_nonzero(np.asarray(smple) < half_w))
    for index, p in enumerate(smple):
        try:
            X[index] = np.concatenate((mlevels[p-half_w: p], mlevels[p+1: p+half_w+1]), axis=0)
            Y[index] = 0 if mlevels[p] < 0.5 else 1
        except ValueError:
            #print(index, p)
            count_errored += 1
    X = X.reshape(list(X.shape) + [1])
    print(count_errored, ' profiles faced error')
    print('dataset balanceness', len(Y[Y > 0]), len(Y[Y==0]))
    return X, Y


def profiler(cnfg, methylations, sample_methylations, datasize, window_size=20, from_file=False, threshold=0.5):
    organism_name = cnfg['organism_name']
    half_w = int(window_size/2)
    X, Y= input_maker(methylations, sample_methylations, int(datasize), window_size, organism_name, from_file, half_w, threshold=threshold)
    return X, Y

def run_experiment(X, Y, x_test=None, y_test=None, window_size=20, test_percent=0.1):
    x_train, x_val, y_train, y_val = train_test_split(X, Y, test_size=test_percent, random_state=None)
    if x_test is None:
        x_train, x_test, y_train, y_test = train_test_split(x_train, y_train, test_size=test_percent, random_state=None)
    model = Sequential()
    model.add(Dense(window_size, activation='relu', input_shape=((window_size,1))))
    model.add(Dropout(0.5))
    model.add(Flatten())
    model.add(Dense(16, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(8, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    opt = SGD(lr=0.001)
    model.compile(loss=keras.losses.binary_crossentropy, optimizer=opt, metrics=['accuracy'])
    with tf.device('/device:GPU:0'):
        model.fit(x_train, y_train, batch_size=32, epochs=20, verbose=0, validation_data=(x_val, y_val))
    y_pred = model.predict(x_test)
    return accuracy_score(y_test, y_pred.round()), len(x_train), len(x_test)

def experiments(config_list, context_list, dataset_size=55556, window_size=20, coverage_threshold=10):
    res = []
    for cnfg in config_list:
        methylations, num_to_chr_dic = pg.get_methylations(cnfg, '', coverage_threshold)
        methylations.insert(0, 'idx', range(0, len(methylations)))
        for context in context_list:
            if len(context) == 0:
                sample_methylations, methylations_remain = meth_dfs_construction(methylations, dataset_size*2)
            else:
                sample_methylations = methylations[methylations['context'] == context]
            X, Y = profiler(cnfg, methylations, sample_methylations, dataset_size, window_size=window_size, from_file=False)
            acc, tr_size, te_size = run_experiment(X, Y, window_size=window_size, test_percent=0.1)
            res_row = [cnfg['organism_name'], context, acc, tr_size, te_size]
            print(res_row)
            res.append(res_row)
            np.savetxt("meth_profiles.csv", res, delimiter=", ", fmt ='% s')
    return res


def cross_cnfg_experiments(tr_te_config_list, context_list, dataset_size=50000, window_size=20, coverage_threshold=10):
    test_dataset_size = 10000
    res = []
    for configs in tr_te_config_list:
        tr_cnfg = configs[0]
        tr_methylations, tr_num_to_chr_dic = pg.get_methylations(tr_cnfg, '', coverage_threshold)
        tr_methylations = tr_methylations.sort_values(["chr", "position"], ascending=(True, True))
        tr_methylations.insert(0, 'idx', range(0, len(tr_methylations)))
        for te_cnfg in configs[1:]:
            te_methylations, te_num_to_chr_dic = pg.get_methylations(te_cnfg, '', coverage_threshold)
            te_methylations = te_methylations.sort_values(["chr", "position"], ascending=(True, True))
            te_methylations.insert(0, 'idx', range(0, len(te_methylations)))
            if tr_cnfg['organism_name'] == te_cnfg['organism_name']:
                continue
            for context in context_list:
                if len(context) == 0:
                    tr_sample_methylations, methylations_remain = meth_dfs_construction(tr_methylations, dataset_size*2)
                    te_sample_methylations, methylations_remain = meth_dfs_construction(te_methylations, test_dataset_size*2)
                else:
                    tr_sample_methylations = tr_methylations[tr_methylations['context'] == context]
                    te_sample_methylations = te_methylations[te_methylations['context'] == context]
                X, Y = profiler(tr_cnfg, tr_methylations, tr_sample_methylations, dataset_size, window_size=window_size, from_file=False)
                X_test, Y_test = profiler(te_cnfg, te_methylations, te_sample_methylations, test_dataset_size, window_size=window_size, from_file=False)
                acc, tr_size, te_size = run_experiment(X, Y, x_test=X_test, y_test=Y_test, window_size=window_size, test_percent=0.1)
                res_row = [tr_cnfg['organism_name'], te_cnfg['organism_name'], context, acc, tr_size, te_size]
                print(res_row)
                res.append(res_row)
                np.savetxt("meth_profiles_cross_cnfg.csv", res, delimiter=", ", fmt ='% s')
    return res


def cross_context_experiments(config_list, context_list, dataset_size=50000, window_size=20, coverage_threshold=10):
    test_dataset_size = 10000
    res = []
    for cnfg in config_list:
        methylations, num_to_chr_dic = pg.get_methylations(cnfg, '', coverage_threshold)
        methylations.insert(0, 'idx', range(0, len(methylations)))
        for tr_context in context_list:
            for te_context in context_list:
                if tr_context == te_context:
                    continue
                if len(tr_context) == 0:
                    methylations_train, methylations_remain = meth_dfs_construction(methylations, 200000)
                    methylations_test = methylations_remain[methylations_remain['context'] == te_context]
                else:
                    methylations_test, methylations_remain = meth_dfs_construction(methylations, 50000)
                    methylations_train = methylations_remain[methylations_remain['context'] == tr_context]
                X, Y = profiler(cnfg, methylations, methylations_train, dataset_size, window_size=window_size, from_file=False)
                print('x train size = ' + str(len(X)))
                X_test, Y_test = profiler(cnfg, methylations, methylations_test, test_dataset_size, window_size=window_size, from_file=False)
                print('x test size = ' + str(len(X_test)))
                acc, tr_size, te_size = run_experiment(X, Y, x_test=X_test, y_test=Y_test, window_size=window_size, test_percent=0.1)
                res_row = [cnfg['organism_name'], te_context, tr_context, acc, tr_size, te_size]
                print(res_row)
                res.append(res_row)
                np.savetxt("meth_profiles_cross_cntxt.csv", res, delimiter=", ", fmt ='% s')
    return res



