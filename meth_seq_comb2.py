import compatibility as compatibility

import data_reader as data_reader
import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Input, concatenate
import random
import numpy as np
from tensorflow import keras
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.layers import Dropout, Flatten, Reshape
from tensorflow.keras.optimizers import SGD
from sklearn.metrics import accuracy_score
import profile_generator as pg
import configs as configs
import preprocess
from os.path import exists

def input_maker(methylations,  datasize, window_size, organism_name, from_file, context, half_w, methylated = True):
    methylations = methylations.sort_values(["chr", "position"], ascending=(True, True))
    chrs_counts = methylations['chr'].value_counts()
    last_chr_pos = {}
    chrnums = list(chrs_counts.index)
    sum = 0
    for i in range(len(chrnums)):
        if i in chrs_counts.keys():
            last_chr_pos[i] = sum+chrs_counts[i]-1
            sum += chrs_counts[i]
    # last_chr_pos ==> {0: 5524, 1: 1042784, 2: 1713034, 3: 2550983, 4: 3205486, 5: 4145381, 6: 4153872}
    # methylations.iloc[2550983] => chr 3.0 position    23459763.0
    # methylations.iloc[2550984] => chr 4.0 position    1007
    methylations.insert(0, 'idx', range(0, len(methylations)))
    sub_methylations = methylations[methylations['context'] == context]
    if len(context) == 0:
        sub_methylations = preprocess.equal_context_meth_subseter(methylations)
    idxs = sub_methylations['idx']
    mlevels = methylations['mlevel']
    mlevels = np.asarray(mlevels)
    X = np.zeros((datasize, window_size))
    Y = np.zeros(datasize)
    if from_file and exists('./temporary_files/' +organism_name+'_meth_avlbls.npy'):
        avlbls = np.load('./temporary_files/' +organism_name+'_meth_avlbls.npy')
    else:
        avlbls = np.asarray(idxs)
        for lcp in list(last_chr_pos.values()):
            if lcp > 0 and lcp < len(mlevels) - window_size:
                avlbls = np.setdiff1d(avlbls, range(lcp-half_w, lcp+half_w))
        np.save('./temporary_files/' +organism_name+'_meth_avlbls.npy', avlbls)
    if methylated:
        filtered_avlbls = [x for x in avlbls if mlevels[x] > 0.5]
    else:
        filtered_avlbls = [x for x in avlbls if mlevels[x] <= 0.5]
    smple = random.sample(list(filtered_avlbls), datasize)
    count_errored = 0
    print('border conditions: ', np.count_nonzero(np.asarray(smple) < half_w))
    for index, p in enumerate(smple):
        try:
            X[index] = np.concatenate((mlevels[p-half_w: p], mlevels[p+1: p+half_w+1]), axis=0)
            Y[index] = 0 if mlevels[p] < 0.5 else 1
        except ValueError:
            print(index, p)
            count_errored += 1
    X = X.reshape(list(X.shape) + [1])
    print(count_errored, ' profiles faced error')
    return X, Y, methylations, smple

# This function is for testing, me: all the methylations data_frame, X_methylated: is input of the network which is a list of lists containing methylation of neighboring cytosins.
# p is the index in the me dataframe, i is the index of X_methylated.
# def check_check(me, X_methylated, p, i):
#     return np.all(np.concatenate([np.asarray(me.iloc[p - 10: p].mlevel), np.asarray(me.iloc[p+1: p+1+ 10].mlevel)]) == X_methylated[i,:,0])

def profiler(cnfg, methylations, context, datasize, sequences_onehot, annot_seqs_onehot, num_to_chr_dic,
             me_window_size=20, seq_window_size=3200, from_file=False, threshold=0.5, include_annot=True, include_repeat=True):
    #methylations = methylations[(methylations['mlevel'] > 0.8) | (methylations['mlevel'] < 0.2)]
    organism_name = cnfg['organism_name']
    half_w = int(me_window_size/2)
    X_methylated, Y_methylated, meth_me, smple_me = input_maker(methylations, int(datasize/2), me_window_size, organism_name, from_file, context, half_w, methylated=True)
    X_unmethylated, Y_unmethylated, meth_ume, smple_ume = input_maker(methylations, int(datasize/2), me_window_size, organism_name, from_file, context, half_w, methylated=False)
    profiles_me, targets_me = pg.get_profiles(meth_me, smple_me, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=seq_window_size)
    X_seq_me, Y_me = pg.data_preprocess(profiles_me, targets_me, include_annot=include_annot | include_repeat)
    profiles_ume, targets_ume = pg.get_profiles(meth_ume, smple_ume, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=seq_window_size)
    X_seq_ume, Y_ume = pg.data_preprocess(profiles_ume, targets_ume, include_annot=include_annot | include_repeat)
    return np.concatenate((X_methylated, X_unmethylated), axis=0), np.concatenate([X_seq_me, X_seq_ume], axis=0), np.concatenate((Y_methylated, Y_unmethylated), axis=0)

def run_experiment(X_seq, X_meth, Y, seq_cols, meth_window_size=20, seq_window_size= 3200,  test_percent=0.2, test_val_percent = 0.5):
    X_seq_train, X_seq_test, X_meth_train, X_meth_test, y_train, y_test = train_test_split(X_seq, X_meth, Y, test_size=test_percent, random_state=42)
    X_seq_test, X_seq_val, X_meth_test, X_meth_val, y_test, y_val = train_test_split(X_seq_test, X_meth_test, y_test, test_size=test_val_percent, random_state=42)
    input_1 = Input(shape=(seq_window_size, seq_cols, 1), name='input_1')
    y = Conv2D(16, kernel_size=(1, seq_cols), padding='VALID', activation='relu')(input_1)
    y = Reshape((80, 40, 16), input_shape=(seq_window_size, 1, 16))(y)
    y = Conv2D(16, kernel_size=(5, 3), padding='VALID', activation='relu')(y)
    y = Flatten()(y)
    input_2 = Input(shape=(meth_window_size), name='input_2')
    x = Dense(meth_window_size, activation='relu', input_shape=((meth_window_size,1)))(input_2)
    x = Dropout(0.5)(x)
    x = Flatten()(x)
    combined = concatenate([x, y])
    x = Dense(16, activation='relu')(combined)
    x = Dropout(0.5)(x)
    x = Dense(8, activation='relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(1, activation='sigmoid')(x)
    opt = SGD(lr=0.001)
    model = tf.keras.models.Model(inputs=[input_1, input_2], outputs=x)
    model.compile(loss=keras.losses.binary_crossentropy, optimizer=opt, metrics=['accuracy'])
    model.fit([X_seq_train, X_meth_train], y_train, batch_size=32, epochs=20, verbose=1, validation_data=([X_seq_val, X_meth_val], y_val))
    y_pred = model.predict([X_seq_test,X_meth_test])
    return accuracy_score(y_test, y_pred.round())

def get_sequences(cnfg, context, coverage_threshold, seq_window_size):
    sequences_onehot, _, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(cnfg, context, coverage_threshold=coverage_threshold, sequneces_df_from_file=True, annotseqs_from_file=True)
    annot_df = data_reader.read_annot(cnfg['repeat_address'])
    sequences = data_reader.readfasta(cnfg['seq_address'])
    organism_name = cnfg['organism_name']
    if organism_name == configs.Cowpea_config['organism_name']:
        sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
        annot_df = compatibility.cowpea_annotation_compatibility(annot_df)
    annot_str = preprocess.make_annotseq_dic(organism_name, 'repeat', annot_df, sequences, from_file=True, strand_spec=False)
    annot_seqs_onehot.append(annot_str)
    PROFILE_COLS = 4
    PROFILE_COLS = PROFILE_COLS + 2*len(cnfg['annot_types'])
    PROFILE_COLS = PROFILE_COLS + 1
    return sequences_onehot, annot_seqs_onehot, num_to_chr_dic, PROFILE_COLS

def experiments(config_list, context_list, dataset_size=50000, meth_window_size=20, seq_window_size=3200, coverage_threshold=10):
    res = []
    for cnfg in config_list:
        methylations_, num_to_chr_dic_ = pg.get_methylations(cnfg, '', coverage_threshold)
        for context in context_list:
            sequences_onehot, annot_seqs_onehot, num_to_chr_dic, seq_cols = get_sequences(cnfg, context, coverage_threshold, seq_window_size)
            X_meth, X_seq, Y = profiler(cnfg, methylations_, context, dataset_size,sequences_onehot, annot_seqs_onehot, num_to_chr_dic_, me_window_size=meth_window_size, seq_window_size=seq_window_size)
            acc = run_experiment(X_seq, X_meth, Y, seq_cols, meth_window_size=meth_window_size, seq_window_size=seq_window_size, test_percent=0.2, test_val_percent=0.5)
            res_row = [cnfg['organism_name'], context, acc]
            print(res_row)
            res.append(res_row)
            np.savetxt("meth__seq_comb_profiles_.csv", res, delimiter=", ", fmt ='% s')
    return res


# with tf.device('/device:GPU:0'):
#     input_1 = Input(shape=(PROFILE_ROWS, PROFILE_COLS, 1), name='input_1')
#     x = Conv2D(16, kernel_size=(1, PROFILE_COLS), padding='VALID', activation='relu')(input_1)
#     x = Reshape((block_sizes[w][0], block_sizes[w][1], 16), input_shape=(PROFILE_ROWS, 1, 16))(x)
# #    x = Conv2D(16, kernel_size=(5, 3), padding='VALID', activation='relu')(x)
#     x = Flatten()(x)
#     input_2 = Input(shape=(meth_window_size), name='input_2')
#     combined = concatenate([x, input_2])
#     y = Dense(16, activation='relu')(combined)
#     y = Dropout(0.5)(y)
#     y = Dense(8, activation='relu')(y)
#     y = Dropout(0.5)(y)
#     y = Dense(1, activation='sigmoid')(y)
#     print('model processed')
#     model = tf.keras.models.Model(inputs=[input_1, input_2], outputs=y)
#     opt = tf.keras.optimizers.Adam(learning_rate=0.001)
#     model.compile(loss=keras.losses.binary_crossentropy, optimizer=opt, metrics=['accuracy'])
