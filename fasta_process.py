import compatibility
import configs
import pandas as pd



def Tomato_seq_process():
    # define a list of chromosomes to keep
    chromosomes_to_keep = compatibility.Tomato_chros
    # open the input and output files
    with open(configs.Tomato_config['seq_address'], 'r') as in_file, open('output.fasta', 'w') as out_file:
        # initialize variables for the current sequence header and sequence
        current_header = ''
        current_sequence = ''
        # iterate through each line in the input file
        for line in in_file:
            line = line.strip()
            # if the line starts with a '>' character, it is a sequence header
            if line.startswith('>'):
                # check if the current sequence header belongs to a chromosome to keep
                header_parts = line.split()
                chromosome = header_parts[0][1:]
                if chromosome in chromosomes_to_keep:
                    # write the previous sequence to the output file (if any)
                    if current_sequence:
                        out_file.write(current_header + '\n')
                        out_file.write(current_sequence + '\n')
                    # store the new sequence header and reset the sequence
                    current_header = line
                    current_sequence = ''
                else:
                    # if the chromosome is not in the list of chromosomes to keep, skip this sequence
                    current_header = ''
                    current_sequence = ''
            else:
                # if the line does not start with a '>', it is sequence data
                current_sequence += line

        # write the last sequence to the output file
        if current_sequence:
            out_file.write(current_header + '\n')
            out_file.write(current_sequence + '\n')

def tomato_methylations_preprocess():
    methylations = pd.read_table(configs.Tomato_config['methylation_address'])
    methylations.columns = ['chr', 'position', 'strand', 'meth', 'unmeth', 'context', 'three']
    values_to_keep = compatibility.Tomato_chros
    filtered_df = methylations[methylations['chr'].isin(values_to_keep)]
    filtered_df.to_csv('output_file_methylations.tsv', sep='\t', index=False, header=None)

def tomato_annot_preprocess():
    annot_df = pd.read_table(configs.Tomato_config['annot_address'], sep='\t', comment='#')
    annot_df.columns = ['chr', 'source', 'type', 'start', 'end', 'score', 'strand', 'phase', 'attributes']
    values_to_keep = compatibility.Tomato_chros
    filtered_df = annot_df[annot_df['chr'].isin(values_to_keep)]
    filtered_df.to_csv('output_file_annot.tsv', sep='\t', index=False, header=None)

def tomato_repeat_preprocess():
    annot_df = pd.read_table(configs.Tomato_config['repeat_address'], sep='\t', comment='#')
    annot_df.columns = ['chr', 'source', 'type', 'start', 'end', 'score', 'strand', 'phase', 'attributes']
    values_to_keep = compatibility.Tomato_chros
    filtered_df = annot_df[annot_df['chr'].isin(values_to_keep)]
    filtered_df.to_csv('output_file_repeat.tsv', sep='\t', index=False, header=None)

Tomato_seq_process()
tomato_methylations_preprocess()
tomato_annot_preprocess()
tomato_repeat_preprocess()
