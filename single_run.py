import compatibility as compatibility
import configs as configs
import numpy as np
import data_reader as data_reader
import preprocess as preprocess
import random
import os
from datetime import datetime
import tensorflow as tf
import numpy as np
import pandas as pd
from tensorflow import keras
from sklearn.model_selection import train_test_split
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape, Input, concatenate
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score, precision_score, recall_score
import configs as configs
from os import path
import profile_generator as pg
import constants

def get_nonzero_neighbors(arr1, arr2, window_size):
    nz_1 = arr1[np.nonzero(arr1)[0]]
    w2 = int(window_size/2)
    if len(nz_1) > w2:
        nz_1 = nz_1[-w2:]
    else:
        nz_1 = np.concatenate([[-1]*(w2 - len(nz_1)),  nz_1])
    nz_2 = arr2[np.nonzero(arr2)[0]]
    if len(nz_2) > w2:
        nz_2 = nz_2[:w2]
    else:
        nz_2 = np.concatenate([nz_2, [-1]*(w2 - len(nz_2))])
    return np.concatenate([nz_1, nz_2])

def make_meth_profiles(methylations_subset, meth_seq, num_to_chr_dic, window_size, interval_size):
    res = methylations_subset.apply(lambda row: get_nonzero_neighbors(meth_seq[num_to_chr_dic[row['chr']]][row['position']-int(interval_size/2):row['position']-1, 0], meth_seq[num_to_chr_dic[row['chr']]][row['position']: row['position']+int(interval_size/2), 0], window_size), axis=1)
    res = np.stack(res.to_numpy())
    res[res == constants.NON_METH_TAG] = 0
    return res

def test_sampler(methylations_test, sequences_onehot, annot_seqs_onehot, window_size, num_to_chr_dic, meth_seq, meth_window_size = 40, include_annot=False, test_sample_size=100000):
    methylated, unmethylated = preprocess.methylations_subseter(methylations_test, window_size)
    test_sample_size = int(min(test_sample_size/2, len(methylated), len(unmethylated)))
    test_sample_set = methylated[:test_sample_size]+unmethylated[:test_sample_size]
    random.shuffle(test_sample_set)
    test_profiles, test_targets = pg.get_profiles(methylations_test, test_sample_set, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=window_size)
    X_seq_test, y_test = pg.data_preprocess(test_profiles, test_targets, include_annot=include_annot)
    X_meth_test = make_meth_profiles(methylations_test.iloc[test_sample_set], meth_seq, num_to_chr_dic, meth_window_size, window_size)
    return X_seq_test, X_meth_test,  y_test

config_list = [configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia]
context_list = ['CG', 'CHG', 'CHH', '']
cnfg = config_list[0]
context = context_list[0]
steps = [0, 500000]
coverage_threshold = 10
include_annot = True
at = None
memory_chunk_size = 10000
need_test = True
include_repeat=True
meth_window_size = 20
window_sizes = [400]
block_sizes = [(20, 20)]
w = 0

organism_name = cnfg['organism_name']
sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(cnfg, context, at=at, coverage_threshold=coverage_threshold, sequneces_df_from_file=True, annotseqs_from_file=True)
# if len(context) == 0:
#     methylations = preprocess.equal_context_meth_subseter(methylations) ##########################
methylations_train, methylations_test = preprocess.seperate_methylations(organism_name, methylations, from_file=True)
annot_df = data_reader.read_annot(cnfg['repeat_address'])
sequences = data_reader.readfasta(cnfg['seq_address'])
# if organism_name == configs.Cowpea_config['organism_name']:
#     sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
#     annot_df = compatibility.cowpea_annotation_compatibility(annot_df)
annot_str = preprocess.make_annotseq_dic(organism_name, 'repeat', annot_df, sequences, from_file=True, strand_spec=False)
annot_seqs_onehot.append(annot_str)
methylations_ = data_reader.read_methylations(cnfg['methylation_address'], '', coverage_threshold=coverage_threshold)
# if organism_name == configs.Cowpea_config['organism_name']:
#     methylations_ = compatibility.cowpea_methylation_compatibility(methylations_)
meth_seq = preprocess.make_meth_string(organism_name, methylations_, sequences_onehot, coverage_threshold, from_file=True)
PROFILE_ROWS = window_sizes[w]
print(len(annot_seqs_onehot))
PROFILE_COLS = 4
PROFILE_COLS = PROFILE_COLS + 2*len(cnfg['annot_types'])
PROFILE_COLS = PROFILE_COLS + 1
print('PROFILE_COLS', PROFILE_COLS)
with tf.device('/device:GPU:0'):
    input_1 = Input(shape=(PROFILE_ROWS, PROFILE_COLS, 1), name='input_1')
    x = Conv2D(16, kernel_size=(1, PROFILE_COLS), padding='VALID', activation='relu')(input_1)
    x = Reshape((block_sizes[w][0], block_sizes[w][1], 16), input_shape=(PROFILE_ROWS, 1, 16))(x)
#    x = Conv2D(16, kernel_size=(5, 3), padding='VALID', activation='relu')(x)
    x = Flatten()(x)
    input_2 = Input(shape=(meth_window_size), name='input_2')
    combined = concatenate([x, input_2])
    y = Dense(16, activation='relu')(combined)
    y = Dropout(0.5)(y)
    y = Dense(8, activation='relu')(y)
    y = Dropout(0.5)(y)
    y = Dense(1, activation='sigmoid')(y)
    print('model processed')
    model = tf.keras.models.Model(inputs=[input_1, input_2], outputs=y)
    opt = tf.keras.optimizers.Adam(learning_rate=0.001)
    model.compile(loss=keras.losses.binary_crossentropy, optimizer=opt, metrics=['accuracy'])
methylated_train, unmethylated_train = preprocess.methylations_subseter(methylations_train, window_sizes[w])
ds_size = min(len(methylated_train), len(unmethylated_train))
x_train_sz = 0
s = 0
step = steps[s+1] - steps[s]
print('##################################', step)
print('#################################', ds_size)
slice = int(steps[s]/2)
print('model fitting started for ' + organism_name + ' ' + context)
print(datetime.now())
chunk = 0
sample_set = methylated_train[chunk:chunk+memory_chunk_size]+unmethylated_train[chunk:chunk+memory_chunk_size]
random.shuffle(sample_set)
profiles, targets = pg.get_profiles(methylations_train, sample_set, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=window_sizes[w])
X_seq, Y = pg.data_preprocess(profiles, targets, include_annot=include_annot | include_repeat)
X_meth = make_meth_profiles(methylations_train.iloc[sample_set], meth_seq, num_to_chr_dic, meth_window_size, window_sizes[w])
X_seq_train, X_seq_val, X_meth_train, X_meth_val, y_train, y_val = train_test_split(X_seq, X_meth, Y, test_size=0.1, random_state=42)
x_train_sz += len(X_seq_train)
with tf.device('/device:GPU:0'):
    model.fit([X_seq_train, X_meth_train], y_train, batch_size=32, epochs=20, verbose=1, validation_data=([X_seq_val, X_meth_val], y_val))

x_seq_test, x_meth_test, y_test = test_sampler(methylations_test, sequences_onehot, annot_seqs_onehot, window_sizes[w], num_to_chr_dic, meth_seq, meth_window_size=meth_window_size, include_annot=include_annot|include_repeat)
y_pred = model.predict([x_seq_test, x_meth_test])
accuracy_score(y_test, y_pred.round())
