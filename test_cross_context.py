import profile_generator as pg

import preprocess as preprocess
import random
from datetime import datetime
import tensorflow as tf
import numpy as np
import pandas as pd
from tensorflow import keras
from tensorflow.keras.layers import Activation,Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Reshape
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, f1_score, precision_score, recall_score
import configs as configs
import data_reader
import compatibility


model_names = [
    "TomatoCHHannot4500003200.mdl",
    "TomatoCHGannot4500003200.mdl",
    "TomatoCGannot4114293200.mdl",
    'Tomatoannot4500003200.mdl',
    "CucumberCHHannot4500003200.mdl",
    "CucumberCHGannot4500003200.mdl",
    "CucumberCGannot4500003200.mdl",
    'Cucumberannot4500003200.mdl',
    "RiceCHHannot4500003200.mdl",
    "RiceCHGannot4500003200.mdl",
    "RiceCGannot4500003200.mdl",
    'Riceannot4500003200.mdl',
    "CowpeaCHHannot4500003200.mdl",
    "CowpeaCHGannot4500003200.mdl",
    "CowpeaCGannot4500003200.mdl",
    'Cowpeaannot4500003200.mdl',
    "ArabidopsisCHHannot1726883200.mdl",
    "ArabidopsisCHGannot4500003200.mdl",
    "ArabidopsisCGannot4500003200.mdl",
    'Arabidopsisannot4500003200.mdl'
]

model_names = ['Tomatocombinedannot9000003200.mdl',
 'TomatoCHHannot6595683200.mdl',
 'TomatoCHGannot9000003200.mdl',
 'TomatoCGannot4117823200.mdl',
 'Cucumbercombinedannot9000003200.mdl',
 'CucumberCHHannot9000003200.mdl',
 'CucumberCHGannot9000003200.mdl',
 'CucumberCGannot9000003200.mdl',
 'Ricecombinedannot9000003200.mdl',
 'RiceCHHannot9000003200.mdl',
 'RiceCHGannot9000003200.mdl',
 'RiceCGannot9000003200.mdl',
 'Cowpeacombinedannot9000003200.mdl',
 'CowpeaCHHannot9000003200.mdl',
 'CowpeaCHGannot9000003200.mdl',
 'CowpeaCGannot9000003200.mdl',
 'Arabidopsiscombinedannot5628923200.mdl',
 'ArabidopsisCHHannot1728053200.mdl',
 'ArabidopsisCHGannot5249933200.mdl',
 'ArabidopsisCGannot9000003200.mdl']

model_names = ['MarchantiaCGannot:True-repeat:True900000_3200.mdl',
               'MarchantiaCHGannot:True-repeat:True875833_3200.mdl',
               'MarchantiaCHHannot:True-repeat:True70545_3200.mdl',
               'Marchantiacombinedannot:True-repeat:True72000_3200.mdl']

model_names = [
    'MarchantiaCGannot:False-repeat:False450000_3200.mdl',
    'MarchantiaCHGannot:False-repeat:False450000_3200.mdl',
    'MarchantiaCHH703353200.mdl',
    'Marchantiacombined2103463200.mdl']


# for cnfg in cnfgs:
#     organism_name = cnfg['organism_name']
#     for context in context_list:
#         for mn in model_names:
#             if organism_name+context+'annot' in mn:
#                 model = keras.models.load_model('./models/' + mn)
#         clst = [c for c in context_list if c != context]
#         for test_context in ['']:
#             sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(cnfg, test_context, coverage_threshold=10)
#             x_test, y_test = pg.test_sampler(methylations, sequences_onehot, annot_seqs_onehot, window_size, num_to_chr_dic, include_annot=include_annot)
#             tag = 'seq-only'
#             if include_annot:
#                 tag = 'seq-annot'
#             y_pred = model.predict(x_test)
#             del sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic
#             context_tag = 'all' if len(context) == 0 else context
#             step_res = [organism_name, context_tag, test_context, tag, window_size, 500000, len(x_test), accuracy_score(y_test, y_pred.round()),
#                                 f1_score(y_test, y_pred.round()), precision_score(y_test, y_pred.round()), recall_score(y_test, y_pred.round())]
#             res.append(step_res)
#             np.savetxt("GFG_cross_context_equal" + tag + ".csv", res, delimiter=", ", fmt='% s')


def cntx_spc_sub_methylations(methylations, threshold=0.5):
    cg_meth = methylations[(methylations['context'] == 'CG') & (methylations['mlevel'] >= threshold)]
    cg_unmeth = methylations[(methylations['context'] == 'CG') & (methylations['mlevel'] < threshold)]
    chg_meth = methylations[(methylations['context'] == 'CHG') & (methylations['mlevel'] >= threshold)]
    chg_unmeth = methylations[(methylations['context'] == 'CHG') & (methylations['mlevel'] < threshold)]
    chh_meth = methylations[(methylations['context'] == 'CHH') & (methylations['mlevel'] >= threshold)]
    chh_unmeth = methylations[(methylations['context'] == 'CHH') & (methylations['mlevel'] < threshold)]
    return {'cg_meth': cg_meth, 'cg_unmeth': cg_unmeth,
            'chg_meth': chg_meth, 'chg_unmeth': chg_unmeth,
            'chh_meth': chh_meth, 'chh_unmeth': chh_unmeth}

def sample_n_subtract(methylations, num):
    methylations = methylations.sample(frac=1).reset_index(drop=True)
    return methylations.iloc[:num], methylations.iloc[num:]


#Making two dataFrames 1)Each context, methylation status has size/6 entries in the dataframe 2)remaining
def meth_dfs_construction(methylations, size):
    meth_subs_dic = cntx_spc_sub_methylations(methylations)
    eq_dfs = []
    res_dfs = []
    for cntx in meth_subs_dic.keys():
        sampled_df, residue_df = sample_n_subtract(meth_subs_dic[cntx], int(size/6))
        eq_dfs.append(sampled_df)
        res_dfs.append(residue_df)
    return pd.concat(eq_dfs), pd.concat(res_dfs)



def run_experiments(cnfg, tr_context, te_context, methylations_train, methylations_test, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, include_annot=True, include_repeat=True, memory_chunk_size=10000):
    organism_name = cnfg['organism_name']
    window_sizes = [3200]
    block_sizes = [(80, 40)]
    w = 0
    steps = [0, 450000]
    s = 0
    if not include_annot:
        del annot_seqs_onehot
        annot_seqs_onehot = []
    PROFILE_ROWS = window_sizes[w]
    PROFILE_COLS = 4
    if include_annot:
        PROFILE_COLS = PROFILE_COLS + 2*len(cnfg['annot_types'])
    if include_repeat:
        PROFILE_COLS = PROFILE_COLS + 1
    model = Sequential()
    model.add(Conv2D(16, kernel_size=(1, PROFILE_COLS), activation='relu', input_shape=(PROFILE_ROWS, PROFILE_COLS, 1)))
    model.add(Reshape((block_sizes[w][0], block_sizes[w][1], 16), input_shape=(PROFILE_ROWS, 1, 16)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    print('model processed')
    print(datetime.now())
    opt = tf.keras.optimizers.SGD(lr=0.01)
    model.compile(loss=keras.losses.binary_crossentropy, optimizer=opt, metrics=['accuracy'])
    methylated_train, unmethylated_train = preprocess.methylations_subseter(methylations_train, window_sizes[w])
    ds_size = min(len(methylated_train), len(unmethylated_train))
    x_train_sz = 0
    step = steps[s+1] - steps[s]
    print('##################################', step)
    print('#################################', ds_size)
    if ds_size * 2 < steps[s+1]: #temporary, does not work for dataset size checking.
        step = (ds_size * 2) - 2
    slice = int(steps[s]/2)
    for chunk in range(slice, slice+int(step/2), memory_chunk_size):
        if chunk+memory_chunk_size > slice+int(step/2):
            sample_set = methylated_train[chunk:slice+int(step/2)]+unmethylated_train[chunk:slice+int(step/2)]
        else:
            sample_set = methylated_train[chunk:chunk+memory_chunk_size]+unmethylated_train[chunk:chunk+memory_chunk_size]
        random.shuffle(sample_set)
        profiles, targets = pg.get_profiles(methylations_train, sample_set, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, window_size=window_sizes[w])
        X, Y = pg.data_preprocess(profiles, targets, include_annot=include_annot)
        x_train, x_val, y_train, y_val = pg.split_data(X, Y, pcnt=0.1)
        x_train_sz += len(x_train)
        with tf.device('/device:GPU:0'):
            model.fit(x_train, y_train, batch_size=32, epochs=45, verbose=0, validation_data=(x_val, y_val))
            del x_train, y_train
    try:
        print('model fitting ended for ' + str(x_train_sz) + ' data')
        print(datetime.now())
    except:
        print(datetime.now())
    x_test, y_test = pg.test_sampler(methylations_test, sequences_onehot, annot_seqs_onehot, window_sizes[w], num_to_chr_dic, include_annot=include_annot)
    y_pred = model.predict(x_test)
    tag = 'seq-only'
    if include_annot:
        tag = 'seq-annot'
    step_res = [organism_name, tr_context, te_context, tag, window_sizes[w], x_train_sz, len(x_test), accuracy_score(y_test, y_pred.round()),
            f1_score(y_test, y_pred.round()), precision_score(y_test, y_pred.round()), recall_score(y_test, y_pred.round())]
    del x_test, y_test
    print(step_res)
    print(datetime.now())
    return step_res



context_list = [
    'CG',
    'CHG',
    'CHH',
    '']

cnfgs = [configs.Marchantia]
include_annot = False
include_repeat = False
window_size = 3200
res = []

tr_te_set = []
for tr_context in context_list:
    for te_context in context_list:
        if tr_context != te_context:
            tr_te_set.append((tr_context, te_context))


trained_models = {}
for cnfg in cnfgs:
    organism_name = cnfg['organism_name']
    for context in context_list:
        for model in model_names:
            if context == '':
                context = 'combined'
            if organism_name in model and context in model:
                trained_models[organism_name + context] = keras.models.load_model('./models/' + model)


res = []
for cnfg in cnfgs:
    organism_name = cnfg['organism_name']
    for tr_te in tr_te_set:
        tr_context = tr_te[0]
        te_context = tr_te[1]
        if len(tr_context) == 0 or len(te_context) == 0:
            sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(cnfg, '', coverage_threshold=10)
            if not include_annot:
                annot_seqs_onehot = []
            if include_repeat:
                annot_df = data_reader.read_annot(cnfg['repeat_address'])
                sequences = data_reader.readfasta(cnfg['seq_address'])
                if organism_name == configs.Cowpea_config['organism_name']:
                    sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
                    annot_df = compatibility.cowpea_annotation_compatibility(annot_df)
                annot_str = preprocess.make_annotseq_dic(organism_name, 'repeat', annot_df, sequences, from_file=True, strand_spec=False)
                annot_seqs_onehot.append(annot_str)
            if len(tr_context) == 0:
                methylations_train, methylations_remain = meth_dfs_construction(methylations, 222414)
                methylations_test = methylations_remain[methylations_remain['context'] == te_context]
            else:
                methylations_test, methylations_remain = meth_dfs_construction(methylations, 50000)
                methylations_train = methylations_remain[methylations_remain['context'] == tr_context]
            step_res = run_experiments(cnfg, tr_context, te_context, methylations_train, methylations_test, sequences_onehot, annot_seqs_onehot, num_to_chr_dic, include_annot=include_annot, include_repeat=include_repeat, memory_chunk_size=10000)
        else:
            model = trained_models[organism_name+tr_context]
            sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(cnfg, te_context, coverage_threshold=10)
            if not include_annot:
                annot_seqs_onehot = []
            if include_repeat:
                    annot_df = data_reader.read_annot(cnfg['repeat_address'])
                    sequences = data_reader.readfasta(cnfg['seq_address'])
                    if organism_name == configs.Cowpea_config['organism_name']:
                        sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
                        annot_df = compatibility.cowpea_annotation_compatibility(annot_df)
                    annot_str = preprocess.make_annotseq_dic(organism_name, 'repeat', annot_df, sequences, from_file=True, strand_spec=False)
                    annot_seqs_onehot.append(annot_str)
            x_test, y_test = pg.test_sampler(methylations, sequences_onehot, annot_seqs_onehot, window_size, num_to_chr_dic, include_annot=include_annot)
            tag = 'seq-only'
            if include_annot:
                tag = 'seq-annot'
                if include_repeat:
                    tag = 'seq-annot-repeat'
            y_pred = model.predict(x_test)
            step_res = [organism_name, tr_context, te_context, tag, window_size, 900000, len(x_test), accuracy_score(y_test, y_pred.round()),
                f1_score(y_test, y_pred.round()), precision_score(y_test, y_pred.round()), recall_score(y_test, y_pred.round())]
        res.append(step_res)
        if include_annot:
            tag = 'annot'
            if include_repeat:
                tag = 'annot_repeat'
        else:
            tag = ''
        np.savetxt("GFG_cross_context" + tag + ".csv", res, delimiter=", ", fmt='% s')


