import configs as configs
import preprocess as preprocess
import numpy as np
import profile_generator as pg
import data_reader as dr

context_list = ['CG', 'CHG', 'CHH', '']
config_list = [configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config]


def get_compartment(methylations, methylated, unmethylated, final_ds_size):
    a = methylated[:int(final_ds_size/2)]
    b = unmethylated[:int(final_ds_size/2)]
    r = []
    for c in context_list:
        r.append(get_context_percentage(methylations.loc[a], c))
    for c in context_list:
        r.append(get_context_percentage(methylations.loc[b], c))
    return r


def get_context_percentage(methylations, context):
    return len(methylations[methylations['context'] == context]) / len(methylations)


def test_compartment_analysis(config_list, context_list):
    res = []
    for config in config_list:
        organism_name = config['organism_name']
        sequences_onehot, methylations, annot_seqs_onehot, num_to_chr_dic = pg.get_processed_data(config, '', coverage_threshold=10)
        methylations_train, methylations_test = preprocess.seperate_methylations(organism_name, methylations, from_file=False)
        methylated_train, unmethylated_train = preprocess.methylations_subseter(methylations_train, 3200)
        methylated_test, unmethylated_test = preprocess.methylations_subseter(methylations_test, 3200)
        ds_size = min(len(methylated_train), len(unmethylated_train))
        final_tr_size = min(500000, 2*ds_size)
        ds_size = min(len(methylated_test), len(unmethylated_test))
        final_te_size = min(50000, 2*ds_size)
        print('start analysis for ' + organism_name)
        for context in context_list:
            tr_compartment = get_compartment(methylations_train, methylated_train, unmethylated_train, final_tr_size)
            r = [organism_name, 'train']
            res.append(r + tr_compartment)
            print(res)
    np.savetxt("context_compartment_methlations.csv", res, delimiter=", ", fmt='% s')


def count_undetermined_Cs(config_list):
    res = []
    for cnfg in config_list:
        organism_name = cnfg['organism_name']
        meth_all = dr.read_methylations(cnfg['methylation_address'], '', coverage_threshold=-1)
        meth_determinded = dr.read_methylations(cnfg['methylation_address'], '', coverage_threshold=10)
        step_res = [organism_name, len(meth_all), len(meth_determinded)]
        res.append(step_res)
        print(step_res)
    np.savetxt("determind_Cs_stat.csv", res, delimiter=", ", fmt='% s')

def methylation_percentage(config_list, context_list, coverage_threshold=10, meth_threshold=0.5):
    res = []
    for cnfg in config_list:
        for context in context_list:
            organism_name = cnfg['organism_name']
            methylations = dr.read_methylations(cnfg['methylation_address'], '', coverage_threshold=coverage_threshold)
            if len(context) > 0:
                sub_meth = methylations[methylations['context'] == context]
            else:
                sub_meth = methylations
            sub_meth['mlevel'] = sub_meth['meth']/(sub_meth['meth']+sub_meth['unmeth'])
            step_res = [organism_name, context, len(sub_meth[sub_meth['mlevel'] >= meth_threshold]) / len(sub_meth)]
            res.append(step_res)
            print(step_res)
    np.savetxt("methylation_percentage_stat.csv", res, delimiter=", ", fmt='% s')

def general_cytosine_stat(cnfg, rc):
    methylations = dr.read_methylations(cnfg['methylation_address'], '', -1)
    res = [len(methylations)]
    for context in ['CG', 'CHG', 'CHH']:
        df = methylations[methylations.context == context]
        res.append(len(df))
        df = df[df.meth+df.unmeth > rc['coverage_threshold']]
        res.append(len(df))
        df = df[df.meth / (df.meth+df.unmeth) > rc['threshold']]
        res.append(len(df))
    return res

