import os

root1 = '/home/csgrads/ssere004/Organisms/'
root2 = '/home/ssere004/Organisms/'
if os.path.isdir(root1):
    root = root1
else:
    root = root2


Arabidopsis_config = {
    'methylation_address': root + 'Arabidopsis/SRR3171614_1_trimmed_bismark_bt2.CX_report.txt',
    'seq_address': root + 'Arabidopsis/GCF_000001735.4_TAIR10.1_genomic.fa',
    'annot_address': root + 'Arabidopsis/GCF_000001735.4_TAIR10.1_genomic.gtf',
    'organism_name': 'Arabidopsis',
    'repeat_address': root+ 'Arabidopsis/repeats/GCF_000001735.4_TAIR10.1_genomic.fa.out.gff',
    'annot_types': ['gene', 'exon', 'CDS'],
    #'annot_types': ['gene'],
    'genome_size': 119668634
}

Cowpea_config = {
    'methylation_address': root + 'Cowpea/2010_1_bismark_bt2_pe.CX_report.txt',
    'seq_address': root + 'Cowpea/Cowpea_Genome_1.0.fasta',
    'annot_address': root + 'Cowpea/Vunguiculata_540_v1.2.gene.gff3',
    'organism_name': 'Cowpea',
    'repeat_address': root + 'Cowpea/repeats/Vunguiculata_IT97K-499-35_v1.2.repeatmasked_assembly_v1.0.gff3',
    'annot_types': ['gene', 'CDS'],
    #'annot_types': ['gene'],
    'genome_size': 519435864
}

Rice_config = {
    'methylation_address': root + 'Rice/SRR618545_final_bismark_bt2.CX_report.txt',
    'seq_address': root + 'Rice/IRGSP-1.0_genome.fasta',
    'annot_address': root + 'Rice/transcripts_exon.gff',
    #'annot_address': root + 'Rice/GCF_001433935.1_IRGSP-1.0_genomic.gff,
    'organism_name': 'Rice',
    'repeat_address': root + 'Rice/repeats/IRGSP-1.0_genome.fasta.out.gff',
    'annot_types': ['mRNA', 'exon'],
    #'annot_types': ['mRNA'],
    #'annot_types': ['gene', 'exon', 'CDS'],
    'genome_size': 373245519
}

Tomato_config = {
    'methylation_address': root + 'Tomato/output_file_methylations.tsv',
    'seq_address': root + 'Tomato/output.fasta',
    'annot_address': root + 'Tomato/output_file_annot.tsv',
    'organism_name': 'Tomato',
    'repeat_address': root + 'Tomato/output_file_repeat.tsv',
    'annot_types': ['gene', 'exon', 'CDS'],
    #'annot_types': ['gene'],
    'chromosomes': ['NC_015438.3', 'NC_015439.3', 'NC_015440.3', 'NC_015441.3', 'NC_015442.3', 'NC_015443.3',
                    'NC_015444.3', 'NC_015445.3', 'NC_015446.3', 'NC_015447.3', 'NC_015448.3', 'NC_015449.3'],
    'genome_size': 226640966
}

Cucumber_config = {
    'methylation_address': root + 'Cucumber/SRR5430777_1_bismark_bt2_pe.CX_report.txt',
    'seq_address': root + 'Cucumber/GCF_000004075.3_Cucumber_9930_V3_genomic.fa',
    'annot_address': root + 'Cucumber/GCF_000004075.3_Cucumber_9930_V3_genomic.gff',
    'organism_name': 'Cucumber',
    'repeat_address': root + 'Cucumber/repeats/GCF_000004075.3_Cucumber_9930_V3_genomic.fa.out.gff',
    'annot_types': ['gene', 'exon', 'CDS'],
    #'annot_types': ['gene'],
    'genome_size': 828349174
}

Physcomitrella_patens = {
    'methylation_address': root + 'Physcomitrella_patens/SRR10694857_FPC1_bismark_bt2_pe.CX_report.txt',
    'seq_address': root + 'Physcomitrella_patens/GCF_000002425.4_Phypa_V3_genomic.fa',
    'annot_address': root + 'Physcomitrella_patens/GCF_000002425.4_Phypa_V3_genomic.gff',
    'organism_name': 'Physcomitrella_patens',
    'repeat_address': root + 'Physcomitrella_patens/repeats.gff',
    'annot_types': ['gene', 'exon', 'CDS'],
    #'annot_types': ['gene'],
    'genome_size': 0
}

Marchantia ={
     'methylation_address': root + 'Marchantia/out.CX_report.txt',
    'seq_address': root + 'Marchantia/GCA_003032435.1_Marchanta_polymorpha_v1_genomic.fa',
    'annot_address': root + 'Marchantia/GCA_003032435.1_Marchanta_polymorpha_v1_genomic.gff',
    'organism_name': 'Marchantia',
    'repeat_address': root + 'Marchantia/repeats.gff',
    'annot_types': ['gene', 'exon', 'CDS'],
    #'annot_types': ['gene'],
    'genome_size': 0
}

Zea_Mays = {
    'methylation_address': root + 'Zea_Mays/SRR12064586_FPC1_bismark_bt2_pe.CX_report.txt',
    'seq_address': root + 'Zea_Mays/GCF_902167145.1_Zm-B73-REFERENCE-NAM-5.0_genomic.fa',
    'annot_address': root + 'Zea_Mays/GCF_902167145.1_Zm-B73-REFERENCE-NAM-5.0_genomic.gff',
    'organism_name': 'Zea_Mays',
    'repeat_address': root + 'Zea_Mays/',
    'annot_types': ['gene', 'exon', 'CDS'],
    'genome_size': 0
}

Arabidopsis_c24_config = {
    'methylation_address': '/home/csgrads/ssere004/Organisms/7accessions/c24/c24_.deduplicated.CX_report.txt',
    'seq_address': '/home/csgrads/ssere004/Organisms/7accessions/c24/c24_as.fa',
    'annot_address': '/home/csgrads/ssere004/Organisms/7accessions/c24/C24.protein-coding.genes.v2.5.2019-10-09.gff3',
    'organism_name': 'Arabidopsis_c24',
    'repeat_address': '/home/csgrads/ssere004/Organisms/7accessions/c24/c24_as.fa.out.gff',
    'annot_types': ['gene', 'exon', 'CDS'],
    #'annot_types': ['gene'],
    'genome_size': 119668634
}


col0_config = {
    'methylation_address': root + '1001/col-0/col-0_me.txt',
    'seq_address': root + '1001/col-0/col-0_as.fa',
    'annot_address': root + '1001/col-0/col-0_an.gff3',
    'organism_name': 'col0',
    'repeat_address': '',
    'annot_types': ['gene', 'exon', 'CDS'],
    'genome_size': -1
}

c24_config = {
    'methylation_address': root + '1001/c24/c24_me.txt',
    'seq_address': root + '1001/c24/c24_as.fa',
    'annot_address': root + '1001/c24/c24_an.gff3',
    'organism_name': 'c24',
    'repeat_address': '',
    'annot_types': ['gene', 'exon', 'CDS'],
    'genome_size': -1
}


run_config = {
    'coverage_threshold': 10,
    'bin_num': 10,
    'threshold': 0.5,
    'flanking_size': 1000,
    'context': None,
    'allow_pickle': True
}
