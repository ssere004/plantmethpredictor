import configs as configs
import pandas as pd
import data_reader
import numpy as np
#from sklearn.ensemble import RandomForestClassifier
#from sklearn import metrics
import compatibility
import argparse

def get_cnfg_by_og(og):
    cnfg_list = [configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia]
    cnfg_dc = {cnfg['organism_name']: cnfg for cnfg in cnfg_list}
    return cnfg_dc[og]

def get_window_seq(assembely, chro, position):
    res = str(assembely[chro][position - 20: position + 21])
    res = res.replace('A', '1 ').replace('C', '2 ').replace('G', '3 ').replace('T', '4 ')
    if len(res) == 82:
        return res
    else:
        print(str(assembely[chro][position - 20: position + 21]))
        return str('-1'*41)

parser = argparse.ArgumentParser()
parser.add_argument('-og', '--organism_name', help='organism_name', required=True)
parser.add_argument('-cntx', '--context', help='context', required=True)
parser.add_argument('-ds', '--data_size', help='data_size', required=False, default=200000)

args = parser.parse_args()

organism_name = args.organism_name
cnfg = get_cnfg_by_og(organism_name)
context = args.context
dss = int(args.data_size)


# cnfg = configs.Arabidopsis_config
# context = 'CG'
# organism_name = cnfg['organism_name']
# dss = 500000

assembly = data_reader.readfasta(cnfg['seq_address'])
if organism_name == configs.Cowpea_config['organism_name']:
    assembly = compatibility.cowpea_sequence_dic_key_compatibility(assembly)


methylations = pd.read_csv(cnfg['methylation_address'], sep='\t')
methylations.columns = ['chr', 'position', 'strand', 'meth', 'unmeth', 'context', 'three']
if organism_name == configs.Cowpea_config['organism_name']:
    methylations = compatibility.cowpea_methylation_compatibility(methylations)
if context != 'all':
    methylations = methylations[methylations.context == context]
methylations = methylations[methylations.meth + methylations.unmeth>=10]
methylations['label'] = methylations.apply(lambda row: 1 if row['meth'] > row['unmeth'] else 0, axis=1)
meth_df = methylations[methylations.label == 1]
unmeth_df = methylations[methylations.label == 0]
if context != 'all':
    meth_df = meth_df.sample(n=min(dss, min(len(meth_df), len(unmeth_df))))
    unmeth_df = unmeth_df.sample(n=min(dss, min(len(meth_df), len(unmeth_df))))
else:
    available_ds = min(meth_df['context'].value_counts().sort_values()[0], unmeth_df['context'].value_counts().sort_values()[0]) * 3
    final_ds_size = min(available_ds, dss)
    meth_df = pd.concat([meth_df[meth_df['context'] == 'CG'].sample(n=int(final_ds_size/3)),
                         meth_df[meth_df['context'] == 'CHG'].sample(n=int(final_ds_size/3)),
                         meth_df[meth_df['context'] == 'CHH'].sample(n=int(final_ds_size/3))])
    unmeth_df = pd.concat([unmeth_df[unmeth_df['context'] == 'CG'].sample(n=int(final_ds_size/3)),
                         unmeth_df[unmeth_df['context'] == 'CHG'].sample(n=int(final_ds_size/3)),
                         unmeth_df[unmeth_df['context'] == 'CHH'].sample(n=int(final_ds_size/3))])
    meth_df = meth_df.sample(frac=1)
    unmeth_df = unmeth_df.sample(frac=1)
    print('CG, CHG, CHH value counts in the meth dataset: ' + str(meth_df['context'].value_counts().to_dict()))
    print('CG, CHG, CHH value counts in the unmeth dataset: ' + str(unmeth_df['context'].value_counts().to_dict()))

meth_df['text'] = meth_df.apply(lambda row: get_window_seq(assembly, row['chr'], row['position']), axis=1)
meth_df = meth_df[['label', 'text']].reset_index(drop=True)
unmeth_df['text'] = unmeth_df.apply(lambda row: get_window_seq(assembly, row['chr'], row['position']), axis=1)
unmeth_df = unmeth_df[['label', 'text']].reset_index(drop=True)

meth_df = meth_df[meth_df['text'] != '-1'*41]
unmeth_df = unmeth_df[unmeth_df['text'] != '-1'*41]

res = pd.concat([meth_df, unmeth_df]).sample(frac=1).reset_index(drop=True)
res = res.dropna()
if res['text'].str.len().nunique() == 1:
    train_df = res.iloc[:int(len(res)*0.8)].copy()
    test_df = res.iloc[int(len(res)*0.8):].copy()
    train_df.to_csv(f"/home/ssere004/MethPrediction/plantmethpredictor/SMEP_inputs/{cnfg['organism_name']}_{context}_train.tsv", sep=',', index=False, header=False)
    test_df.to_csv(f"/home/ssere004/MethPrediction/plantmethpredictor/SMEP_inputs/{cnfg['organism_name']}_{context}_test.tsv", sep=',', index=False, header=False)
    print('Saved for ' + organism_name + ' ' + context + 'and ratio of 0/1 labels is ' + str(np.sum(train_df.label) / len(train_df)))
else:
    print('ERROR, Nothing Saved cause not all the seqs were the same size.')
