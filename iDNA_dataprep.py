import configs as configs
import pandas as pd
import data_reader
import numpy as np
#from sklearn.ensemble import RandomForestClassifier
#from sklearn import metrics
import compatibility
import argparse

#python /home/ssere004/MethPrediction/plantmethpredictor/iDNA_dataprep.py -og Arabidopsis -cntx all -ds 10000

def get_cnfg_by_og(og):
    cnfg_list = [configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia]
    cnfg_dc = {cnfg['organism_name']: cnfg for cnfg in cnfg_list}
    return cnfg_dc[og]

def get_window_seq(assembely, chro, position):
    res = str(assembely[chro][position - 35: position + 36])
    if len(res) == 71:
        return res
    else:
        return str('A'*71)

parser = argparse.ArgumentParser()
parser.add_argument('-og', '--organism_name', help='organism_name', required=True)
parser.add_argument('-cntx', '--context', help='context', required=True)
parser.add_argument('-ds', '--data_size', help='data_size', required=False, default=50000)

args = parser.parse_args()

organism_name = args.organism_name
cnfg = get_cnfg_by_og(organism_name)
context = args.context
dss = int(args.data_size)


# cnfg = configs.Cowpea_config
# context = 'CG'
# organism_name = cnfg['organism_name']

assembly = data_reader.readfasta(cnfg['seq_address'])
if organism_name == configs.Cowpea_config['organism_name']:
    assembly = compatibility.cowpea_sequence_dic_key_compatibility(assembly)
methylations = pd.read_csv(cnfg['methylation_address'], sep='\t')
methylations.columns = ['chr', 'position', 'strand', 'meth', 'unmeth', 'context', 'three']
if organism_name == configs.Cowpea_config['organism_name']:
    methylations = compatibility.cowpea_methylation_compatibility(methylations)
if context != 'all':
    methylations = methylations[methylations.context == context]
methylations = methylations[methylations.meth + methylations.unmeth>=10]
methylations['label'] = methylations.apply(lambda row: 1 if row['meth'] > row['unmeth'] else 0, axis=1)
meth_df = methylations[methylations.label == 1]
unmeth_df = methylations[methylations.label == 0]
if context != 'all':
    meth_df = meth_df.sample(n=min(dss, min(len(meth_df), len(unmeth_df))))
    unmeth_df = unmeth_df.sample(n=min(dss, min(len(meth_df), len(unmeth_df))))
else:
    available_ds = min(meth_df['context'].value_counts().sort_values()[0], unmeth_df['context'].value_counts().sort_values()[0]) * 3
    final_ds_size = min(available_ds, dss)
    meth_df = pd.concat([meth_df[meth_df['context'] == 'CG'].sample(n=int(final_ds_size/3)),
                         meth_df[meth_df['context'] == 'CHG'].sample(n=int(final_ds_size/3)),
                         meth_df[meth_df['context'] == 'CHH'].sample(n=int(final_ds_size/3))])
    unmeth_df = pd.concat([unmeth_df[unmeth_df['context'] == 'CG'].sample(n=int(final_ds_size/3)),
                         unmeth_df[unmeth_df['context'] == 'CHG'].sample(n=int(final_ds_size/3)),
                         unmeth_df[unmeth_df['context'] == 'CHH'].sample(n=int(final_ds_size/3))])
    meth_df = meth_df.sample(frac=1)
    unmeth_df = unmeth_df.sample(frac=1)
    print('CG, CHG, CHH value counts in the meth dataset: ' + str(meth_df['context'].value_counts().to_dict()))
    print('CG, CHG, CHH value counts in the unmeth dataset: ' + str(unmeth_df['context'].value_counts().to_dict()))

meth_df['text'] = meth_df.apply(lambda row: get_window_seq(assembly, row['chr'], row['position']), axis=1)
meth_df = meth_df[['label', 'text']].reset_index(drop=True)
unmeth_df['text'] = unmeth_df.apply(lambda row: get_window_seq(assembly, row['chr'], row['position']), axis=1)
unmeth_df = unmeth_df[['label', 'text']].reset_index(drop=True)

res = pd.concat([meth_df, unmeth_df]).sample(frac=1).reset_index(drop=True)
res = res.dropna()
if res['text'].str.len().nunique() == 1:
    train_df = res.iloc[:int(len(res)*0.8)].copy()
    test_df = res.iloc[int(len(res)*0.8):].copy()
    train_df['index'] = range(len(train_df))
    test_df['index'] = range(len(test_df))
    train_df = train_df[['index', 'label', 'text']]
    test_df = test_df[['index', 'label', 'text']]
    train_df.to_csv(f"/home/ssere004/MethPrediction/plantmethpredictor/iDNA_inputs/{cnfg['organism_name']}_{context}_train.tsv", sep='\t', index=False)
    test_df.to_csv(f"/home/ssere004/MethPrediction/plantmethpredictor/iDNA_inputs/{cnfg['organism_name']}_{context}_test.tsv", sep='\t', index=False)
    print('Saved for ' + organism_name + ' ' + context + 'and ratio of 0/1 labels is ' + str(np.sum(train_df.label) / len(train_df)))
else:
    print('ERROR, Nothing Saved cause not all the seqs were the same size.')


def one_hot_encode_sequence(sequence):
    encoding = np.zeros((len(sequence), 4))
    nucleotide_dict = {'A': 0, 'C': 1, 'G': 2, 'T': 3, 'N':0}
    for i, nucleotide in enumerate(sequence):
        encoding[i, nucleotide_dict[nucleotide]] = 1
    return encoding

def make_input_df(df):
    X = df.apply(lambda row: one_hot_encode_sequence(row['text']), axis=1)
    X = np.stack(X, axis=0)
    X = np.expand_dims(X, 3)
    Y = df['label']
    return X, Y

# def train_test_rf(x_train, y_train, x_test, y_test):
#     nsamples, nx, ny, nz = x_train.shape
#     x_train = x_train.reshape((nsamples, nx*ny))
#     clf = RandomForestClassifier(random_state=0, n_estimators=50, warm_start=True, n_jobs=-1)
#     clf.fit(x_train, y_train)
#     nsamples, nx, ny, nz = x_test.shape
#     x_test = x_test.reshape((nsamples, nx*ny))
#     y_pred=clf.predict(x_test)
#     return metrics.accuracy_score(y_test, y_pred)

# def run_rf():
#     train_df = pd.read_csv('~/MethPredictor/plantmethpredictor/iDNA_inputs/Arabidopsis_CG_train.tsv', sep='\t')
#     test_df = pd.read_csv('~/MethPredictor/plantmethpredictor/iDNA_inputs/Arabidopsis_CG_test.tsv', sep='\t')
#     x_train, y_train = make_input_df(train_df)
#     x_test, y_test = make_input_df(test_df)
#     print(train_test_rf(x_train, y_train ,x_test, y_test))

