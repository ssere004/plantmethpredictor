import mrcnn
import configs as configs
import numpy as np
config_list = [configs.Arabidopsis_config, configs.Cowpea_config, configs.Rice_config, configs.Cucumber_config, configs.Tomato_config, configs.Marchantia]
#config_list = [configs.Marchantia]
contexts = ['CG', 'CHG', 'CHH', '']

#cnfg = config_list[0]
res = []
for cnfg in config_list:
    for context in contexts:
        acc, f1, precision, recall = mrcnn.run_experiment(cnfg, context, include_annot=False, include_repeat=False)
        res.append([cnfg['organism_name'], context, acc, f1, precision, recall])
        print('################################')
        print([cnfg['organism_name'], context, acc, f1, precision, recall])
        np.savetxt("GFG_mrcnn.csv", res, delimiter=", ", fmt='% s')
