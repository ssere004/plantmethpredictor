import pandas as pd
import constants
import data_reader
import configs
import compatibility
import preprocess

def get_avr(counts, averages):
    sum = 0,
    count = 0
    for i in range(len(counts)):
        count += counts[i]
        sum += (averages[i] * counts[i])
    return sum/count

def get_meth_percentages(mlevels, annot_seq):
    counts_pos = []
    averaged_pos = []
    counts_neg = []
    averaged_neg = []
    for chr in mlevels.keys():
        df = pd.DataFrame({'mlevel': mlevels[chr], 'annot': annot_seq[chr]})
        df = df[df['mlevel'] != 0]
        df['mlevel'] = df['mlevel'].replace(constants.NON_METH_TAG, 0)
        df['mlevel'] = df['mlevel'].abs()
        pos_sub = df[df['annot'] == 1]
        neg_sub = df[df['annot'] == 0]
        counts_pos.append(len(pos_sub))
        counts_neg.append(len(neg_sub))
        averaged_pos.append(pos_sub['mlevel'].mean())
        averaged_neg.append(neg_sub['mlevel'].mean())
    return get_avr(counts_pos, averaged_pos), get_avr(counts_neg, averaged_neg)



def get_methylations(cnfg, context, coverage_threshold):
    organism_name = cnfg['organism_name']
    methylations = data_reader.read_methylations(cnfg['methylation_address'], context, coverage_threshold=coverage_threshold)
    if organism_name == configs.Cowpea_config['organism_name']:
        methylations = compatibility.cowpea_methylation_compatibility(methylations)
    if len(context) != 0:
        methylations, num_to_chr_dic = preprocess.shrink_methylation(methylations)
    else:
        methylations, num_to_chr_dic = preprocess.shrink_methylation(methylations, include_context=True)
    return methylations, num_to_chr_dic

def calc_meth_specifity(cnfg, annot_df, context, coverage_threshold):
    organism_name = cnfg['organism_name']
    sequences = data_reader.readfasta(cnfg['seq_address']) #Can get shrinked the size.
    if organism_name == configs.Cowpea_config['organism_name']:
        sequences = compatibility.cowpea_sequence_dic_key_compatibility(sequences)
        annot_df = compatibility.cowpea_annotation_compatibility(annot_df)
    methylations, num_to_chr_dic = get_methylations(cnfg, context, coverage_threshold)
    annot_str = preprocess.make_annotseq_dic(organism_name, '', annot_df, sequences, from_file=False)
    meth_seq = data_reader.make_meth_string(organism_name, methylations, sequences, coverage_threshold, from_file=False)

    return get_meth_percentages(meth_seq, annot_str)

